package eu.headcrashing.jfs2018;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.CompletionException;

import javax.ws.rs.JAXRS;
import javax.ws.rs.core.Application;

public final class Demo {

	public static final void main(final String[] args) throws GeneralSecurityException, IOException {

		final Application application = new HelloWorldApplication();

		final JAXRS.Configuration configuration = JAXRS.Configuration.builder().build();

		JAXRS.start(application, configuration).thenCompose(instance -> {
			try {
				final JAXRS.Configuration conf = instance.configuration();

				System.out.printf("Instance %s running at %s://%s:%d%s [Native handle: %s].%n", instance,
						conf.protocol().toLowerCase(), conf.host(), conf.port(), conf.rootPath(),
						instance.unwrap(Object.class));
				System.out.println("Press any key to shutdown.");
				System.in.read();

				return instance.stop();
			} catch (final IOException e) {
				throw new CompletionException(e);
			}
		}).thenAccept(stopResult -> System.out.printf("Stop result: %s [Native stop result: %s].%n", stopResult,
				stopResult.unwrap(Object.class))).toCompletableFuture().join();
	}

}
