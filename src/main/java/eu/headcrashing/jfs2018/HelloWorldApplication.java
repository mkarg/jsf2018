package eu.headcrashing.jfs2018;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.core.Application;

public class HelloWorldApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		return Collections.singleton(HelloWorldRessource.class);
	}

}
