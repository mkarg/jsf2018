package eu.headcrashing.jfs2018;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("hello")
public class HelloWorldRessource {

	@GET
	public String sayHello() {
		return "Hello, World!";
	}

}
