package org.jboss.resteasy.spi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Priority;
import javax.net.ssl.SSLContext;
import javax.ws.rs.ConstrainedTo;
import javax.ws.rs.Consumes;
import javax.ws.rs.JAXRS;
import javax.ws.rs.Priorities;
import javax.ws.rs.Produces;
import javax.ws.rs.RuntimeType;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.client.RxInvokerProvider;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Configurable;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Variant;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Providers;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.RuntimeDelegate;
import javax.ws.rs.ext.WriterInterceptor;

import org.eclipse.microprofile.rest.client.ext.ResponseExceptionMapper;
import org.jboss.resteasy.annotations.interception.ClientInterceptor;
import org.jboss.resteasy.annotations.interception.DecoderPrecedence;
import org.jboss.resteasy.annotations.interception.EncoderPrecedence;
import org.jboss.resteasy.annotations.interception.HeaderDecoratorPrecedence;
import org.jboss.resteasy.annotations.interception.RedirectPrecedence;
import org.jboss.resteasy.annotations.interception.SecurityPrecedence;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.client.core.ClientErrorInterceptor;
import org.jboss.resteasy.client.exception.mapper.ClientExceptionMapper;
import org.jboss.resteasy.core.InjectorFactoryImpl;
import org.jboss.resteasy.core.MediaTypeMap;
import org.jboss.resteasy.core.interception.ClientResponseFilterRegistry;
import org.jboss.resteasy.core.interception.ContainerRequestFilterRegistry;
import org.jboss.resteasy.core.interception.ContainerResponseFilterRegistry;
import org.jboss.resteasy.core.interception.InterceptorRegistry;
import org.jboss.resteasy.core.interception.JaxrsInterceptorRegistry;
import org.jboss.resteasy.core.interception.LegacyPrecedence;
import org.jboss.resteasy.core.interception.ReaderInterceptorRegistry;
import org.jboss.resteasy.core.interception.WriterInterceptorRegistry;
import org.jboss.resteasy.core.interception.jaxrs.ClientRequestFilterRegistry;
import org.jboss.resteasy.plugins.delegates.CacheControlDelegate;
import org.jboss.resteasy.plugins.delegates.CookieHeaderDelegate;
import org.jboss.resteasy.plugins.delegates.DateDelegate;
import org.jboss.resteasy.plugins.delegates.EntityTagDelegate;
import org.jboss.resteasy.plugins.delegates.LinkDelegate;
import org.jboss.resteasy.plugins.delegates.LinkHeaderDelegate;
import org.jboss.resteasy.plugins.delegates.LocaleDelegate;
import org.jboss.resteasy.plugins.delegates.MediaTypeHeaderDelegate;
import org.jboss.resteasy.plugins.delegates.NewCookieHeaderDelegate;
import org.jboss.resteasy.plugins.delegates.UriHeaderDelegate;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.plugins.server.netty.NettyJaxrsServer;
import org.jboss.resteasy.resteasy_jaxrs.i18n.LogMessages;
import org.jboss.resteasy.resteasy_jaxrs.i18n.Messages;
import org.jboss.resteasy.specimpl.LinkBuilderImpl;
import org.jboss.resteasy.specimpl.ResponseBuilderImpl;
import org.jboss.resteasy.specimpl.ResteasyUriBuilder;
import org.jboss.resteasy.specimpl.VariantListBuilderImpl;
import org.jboss.resteasy.spi.interception.ClientExecutionInterceptor;
import org.jboss.resteasy.spi.interception.MessageBodyReaderInterceptor;
import org.jboss.resteasy.spi.interception.MessageBodyWriterInterceptor;
import org.jboss.resteasy.spi.interception.PostProcessInterceptor;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;
import org.jboss.resteasy.util.FeatureContextDelegate;
import org.jboss.resteasy.util.PickConstructor;
import org.jboss.resteasy.util.ThreadLocalStack;
import org.jboss.resteasy.util.Types;

/**
 * @author <a href="mailto:bill@burkecentral.com">Bill Burke</a>
 * @version $Revision: 1 $
 */
@SuppressWarnings("unchecked")
public class ResteasyProviderFactory extends RuntimeDelegate
		implements Providers, HeaderValueProcessor, Configurable<ResteasyProviderFactory>, Configuration {
	public static final boolean EE8_PREVIEW_MODE;
	static {
		boolean sseAvailable = false;
		try {
			Class.forName("javax.ws.rs.sse.Sse");
			sseAvailable = true;
		} catch (final ClassNotFoundException e) {
			// ignore
		}
		EE8_PREVIEW_MODE = sseAvailable;
	}

	/**
	 * Allow us to sort message body implementations that are more specific for
	 * their types i.e. MessageBodyWriter<Object> is less specific than
	 * MessageBodyWriter<String>.
	 * <p/>
	 * This helps out a lot when the desired media type is a wildcard and to weed
	 * out all the possible default mappings.
	 */
	protected static class SortedKey<T> implements Comparable<SortedKey<T>>, MediaTypeMap.Typed {
		public Class<?> readerClass;
		public T obj;

		public boolean isBuiltin = false;

		public Class<?> template = null;

		public int priority = Priorities.USER;

		protected SortedKey(final Class<?> intf, final T reader, final Class<?> readerClass, final int priority,
				final boolean isBuiltin) {
			this(intf, reader, readerClass);
			this.priority = priority;
			this.isBuiltin = isBuiltin;
		}

		protected SortedKey(final Class<?> intf, final T reader, final Class<?> readerClass, final boolean isBuiltin) {
			this(intf, reader, readerClass);
			this.isBuiltin = isBuiltin;
		}

		protected SortedKey(final Class<?> intf, final T reader, final Class<?> readerClass) {
			this.readerClass = readerClass;
			this.obj = reader;
			// check the super class for the generic type 1st
			this.template = Types.getTemplateParameterOfInterface(readerClass, intf);
			if (this.template == null)
				this.template = Object.class;
		}

		@Override
		public int compareTo(final SortedKey<T> tMessageBodyKey) {
			// Sort user provider before builtins
			if (this == tMessageBodyKey)
				return 0;
			if (this.isBuiltin == tMessageBodyKey.isBuiltin) {
				if (this.priority < tMessageBodyKey.priority)
					return -1;
				if (this.priority == tMessageBodyKey.priority)
					return 0;
				if (this.priority > tMessageBodyKey.priority)
					return 1;
			}
			if (this.isBuiltin)
				return 1;
			else
				return -1;
		}

		@Override
		public Class<?> getType() {
			return this.template;
		}

		public T getObj() {
			return this.obj;
		}
	}

	protected static class ExtSortedKey<T> extends SortedKey<T> {
		protected ExtSortedKey(final Class<?> intf, final T reader, final Class<?> readerClass, final int priority,
				final boolean isBuiltin) {
			super(intf, reader, readerClass, priority, isBuiltin);
		}

		protected ExtSortedKey(final Class<?> intf, final T reader, final Class<?> readerClass,
				final boolean isBuiltin) {
			super(intf, reader, readerClass, isBuiltin);
		}

		protected ExtSortedKey(final Class<?> intf, final T reader, final Class<?> readerClass) {
			super(intf, reader, readerClass);
		}

		@Override
		public int compareTo(final SortedKey<T> tMessageBodyKey) {
			final int c = super.compareTo(tMessageBodyKey);
			if (c != 0)
				return c;
			if (this.obj == tMessageBodyKey.obj)
				return 0;
			return -1;
		}
	}

	protected static AtomicReference<ResteasyProviderFactory> pfr = new AtomicReference<>();
	protected static ThreadLocalStack<Map<Class<?>, Object>> contextualData = new ThreadLocalStack<>();
	protected static int maxForwards = 20;
	protected static volatile ResteasyProviderFactory instance;
	public static boolean registerBuiltinByDefault = true;

	protected MediaTypeMap<SortedKey<MessageBodyReader>> serverMessageBodyReaders;
	protected MediaTypeMap<SortedKey<MessageBodyWriter>> serverMessageBodyWriters;
	protected MediaTypeMap<SortedKey<MessageBodyReader>> clientMessageBodyReaders;
	protected MediaTypeMap<SortedKey<MessageBodyWriter>> clientMessageBodyWriters;
	protected Map<Class<?>, SortedKey<ExceptionMapper>> sortedExceptionMappers;
	protected Map<Class<?>, ExceptionMapper> exceptionMappers;
	protected Map<Class<?>, ClientExceptionMapper> clientExceptionMappers;
	protected Map<Class<?>, AsyncResponseProvider> asyncResponseProviders;
	protected Map<Class<?>, AsyncStreamProvider> asyncStreamProviders;
	protected Map<Class<?>, MediaTypeMap<SortedKey<ContextResolver>>> contextResolvers;
	protected Map<Class<?>, StringConverter> stringConverters;
	protected Set<ExtSortedKey<ParamConverterProvider>> sortedParamConverterProviders;
	protected List<ParamConverterProvider> paramConverterProviders;
	protected Map<Class<?>, Class<? extends StringParameterUnmarshaller>> stringParameterUnmarshallers;
	protected Map<Class<?>, Map<Class<?>, Integer>> classContracts;

	protected Map<Class<?>, HeaderDelegate> headerDelegates;

	protected LegacyPrecedence precedence;
	protected ReaderInterceptorRegistry serverReaderInterceptorRegistry;
	protected WriterInterceptorRegistry serverWriterInterceptorRegistry;
	protected ContainerRequestFilterRegistry containerRequestFilterRegistry;
	protected ContainerResponseFilterRegistry containerResponseFilterRegistry;

	protected ClientRequestFilterRegistry clientRequestFilterRegistry;

	@Deprecated // variable is maintained for jaxrs-leagcy code support only
	protected JaxrsInterceptorRegistry<ClientRequestFilter> clientRequestFilters;
	protected ClientResponseFilterRegistry clientResponseFilters;
	protected ReaderInterceptorRegistry clientReaderInterceptorRegistry;
	protected WriterInterceptorRegistry clientWriterInterceptorRegistry;
	protected InterceptorRegistry<ClientExecutionInterceptor> clientExecutionInterceptorRegistry;

	protected List<ClientErrorInterceptor> clientErrorInterceptors;

	protected boolean builtinsRegistered = false;
	protected boolean registerBuiltins = true;

	protected InjectorFactory injectorFactory;
	protected ResteasyProviderFactory parent;

	protected Set<DynamicFeature> serverDynamicFeatures;
	protected Set<DynamicFeature> clientDynamicFeatures;
	protected Set<Feature> enabledFeatures;
	protected Map<String, Object> properties;
	protected Set<Class<?>> providerClasses;
	protected Set<Object> providerInstances;
	protected Set<Class<?>> featureClasses;
	protected Set<Object> featureInstances;

	public ResteasyProviderFactory() {
		// NOTE!!! It is important to put all initialization into initialize() as
		// ThreadLocalResteasyProviderFactory
		// subclasses and delegates to this class.
		this.initialize();
	}

	/**
	 * Copies a specific component registry when a new provider is added. Otherwise
	 * delegates to the parent.
	 *
	 * @param parent
	 */
	public ResteasyProviderFactory(final ResteasyProviderFactory parent) {
		this(parent, false);
	}

	/**
	 * If local is true, copies components needed by client configuration, so that
	 * parent is not referenced.
	 *
	 * @param parent
	 * @param local
	 */
	public ResteasyProviderFactory(final ResteasyProviderFactory parent, final boolean local) {
		this.parent = parent;
		this.featureClasses = new CopyOnWriteArraySet<>();
		this.featureInstances = new CopyOnWriteArraySet<>();
		this.providerClasses = new CopyOnWriteArraySet<>();
		this.providerInstances = new CopyOnWriteArraySet<>();
		this.properties = new ConcurrentHashMap<>();
		this.properties.putAll(parent.getProperties());
		this.enabledFeatures = new CopyOnWriteArraySet<>();

		if (local) {
			this.classContracts = new ConcurrentHashMap<>();
			if (parent != null) {
				this.providerClasses.addAll(parent.providerClasses);
				this.providerInstances.addAll(parent.providerInstances);
				this.classContracts.putAll(parent.classContracts);
				this.properties.putAll(parent.properties);
				this.enabledFeatures.addAll(parent.enabledFeatures);
			}
		}
	}

	protected void initialize() {
		this.serverDynamicFeatures = new CopyOnWriteArraySet<>();
		this.clientDynamicFeatures = new CopyOnWriteArraySet<>();
		this.enabledFeatures = new CopyOnWriteArraySet<>();
		this.properties = new ConcurrentHashMap<>();
		this.featureClasses = new CopyOnWriteArraySet<>();
		this.featureInstances = new CopyOnWriteArraySet<>();
		this.providerClasses = new CopyOnWriteArraySet<>();
		this.providerInstances = new CopyOnWriteArraySet<>();
		this.classContracts = new ConcurrentHashMap<>();
		this.serverMessageBodyReaders = new MediaTypeMap<>();
		this.serverMessageBodyWriters = new MediaTypeMap<>();
		this.clientMessageBodyReaders = new MediaTypeMap<>();
		this.clientMessageBodyWriters = new MediaTypeMap<>();
		this.sortedExceptionMappers = new ConcurrentHashMap<>();
		this.exceptionMappers = new ConcurrentHashMap<>();
		this.clientExceptionMappers = new ConcurrentHashMap<>();
		this.asyncResponseProviders = new ConcurrentHashMap<>();
		this.asyncStreamProviders = new ConcurrentHashMap<>();
		this.contextResolvers = new ConcurrentHashMap<>();
		this.sortedParamConverterProviders = Collections
				.synchronizedSortedSet(new TreeSet<ExtSortedKey<ParamConverterProvider>>());
		this.stringConverters = new ConcurrentHashMap<>();
		this.stringParameterUnmarshallers = new ConcurrentHashMap<>();

		this.headerDelegates = new ConcurrentHashMap<>();

		this.precedence = new LegacyPrecedence();
		this.serverReaderInterceptorRegistry = new ReaderInterceptorRegistry(this, this.precedence);
		this.serverWriterInterceptorRegistry = new WriterInterceptorRegistry(this, this.precedence);
		this.containerRequestFilterRegistry = new ContainerRequestFilterRegistry(this, this.precedence);
		this.containerResponseFilterRegistry = new ContainerResponseFilterRegistry(this, this.precedence);

		this.clientRequestFilterRegistry = new ClientRequestFilterRegistry(this);
		this.clientRequestFilters = new JaxrsInterceptorRegistry<>(this, ClientRequestFilter.class);
		this.clientResponseFilters = new ClientResponseFilterRegistry(this);
		this.clientReaderInterceptorRegistry = new ReaderInterceptorRegistry(this, this.precedence);
		this.clientWriterInterceptorRegistry = new WriterInterceptorRegistry(this, this.precedence);
		this.clientExecutionInterceptorRegistry = new InterceptorRegistry<>(ClientExecutionInterceptor.class, this);

		this.clientErrorInterceptors = new CopyOnWriteArrayList<>();

		this.builtinsRegistered = false;
		this.registerBuiltins = true;

		this.injectorFactory = new InjectorFactoryImpl();
		this.registerDefaultInterceptorPrecedences();
		this.addHeaderDelegate(MediaType.class, new MediaTypeHeaderDelegate());
		this.addHeaderDelegate(NewCookie.class, new NewCookieHeaderDelegate());
		this.addHeaderDelegate(Cookie.class, new CookieHeaderDelegate());
		this.addHeaderDelegate(URI.class, new UriHeaderDelegate());
		this.addHeaderDelegate(EntityTag.class, new EntityTagDelegate());
		this.addHeaderDelegate(CacheControl.class, new CacheControlDelegate());
		this.addHeaderDelegate(Locale.class, new LocaleDelegate());
		this.addHeaderDelegate(LinkHeader.class, new LinkHeaderDelegate());
		this.addHeaderDelegate(javax.ws.rs.core.Link.class, new LinkDelegate());
		this.addHeaderDelegate(Date.class, new DateDelegate());
	}

	public Set<DynamicFeature> getServerDynamicFeatures() {
		if (this.serverDynamicFeatures == null && this.parent != null)
			return this.parent.getServerDynamicFeatures();
		return this.serverDynamicFeatures;
	}

	public Set<DynamicFeature> getClientDynamicFeatures() {
		if (this.clientDynamicFeatures == null && this.parent != null)
			return this.parent.getClientDynamicFeatures();
		return this.clientDynamicFeatures;
	}

	protected MediaTypeMap<SortedKey<MessageBodyReader>> getServerMessageBodyReaders() {
		if (this.serverMessageBodyReaders == null && this.parent != null)
			return this.parent.getServerMessageBodyReaders();
		return this.serverMessageBodyReaders;
	}

	protected MediaTypeMap<SortedKey<MessageBodyWriter>> getServerMessageBodyWriters() {
		if (this.serverMessageBodyWriters == null && this.parent != null)
			return this.parent.getServerMessageBodyWriters();
		return this.serverMessageBodyWriters;
	}

	protected MediaTypeMap<SortedKey<MessageBodyReader>> getClientMessageBodyReaders() {
		if (this.clientMessageBodyReaders == null && this.parent != null)
			return this.parent.getClientMessageBodyReaders();
		return this.clientMessageBodyReaders;
	}

	protected MediaTypeMap<SortedKey<MessageBodyWriter>> getClientMessageBodyWriters() {
		if (this.clientMessageBodyWriters == null && this.parent != null)
			return this.parent.getClientMessageBodyWriters();
		return this.clientMessageBodyWriters;
	}

	public Map<Class<?>, ExceptionMapper> getExceptionMappers() {
		if (this.exceptionMappers != null)
			return this.exceptionMappers;
		final Map<Class<?>, ExceptionMapper> map = new ConcurrentHashMap<>();
		for (final Entry<Class<?>, SortedKey<ExceptionMapper>> entry : this.getSortedExceptionMappers().entrySet())
			map.put(entry.getKey(), entry.getValue().getObj());
		this.exceptionMappers = map;
		return map;
	}

	protected Map<Class<?>, SortedKey<ExceptionMapper>> getSortedExceptionMappers() {
		if (this.sortedExceptionMappers == null && this.parent != null)
			return this.parent.getSortedExceptionMappers();
		return this.sortedExceptionMappers;
	}

	protected Map<Class<?>, ClientExceptionMapper> getClientExceptionMappers() {
		if (this.clientExceptionMappers == null && this.parent != null)
			return this.parent.getClientExceptionMappers();
		return this.clientExceptionMappers;
	}

	public Map<Class<?>, AsyncResponseProvider> getAsyncResponseProviders() {
		if (this.asyncResponseProviders == null && this.parent != null)
			return this.parent.getAsyncResponseProviders();
		return this.asyncResponseProviders;
	}

	public Map<Class<?>, AsyncStreamProvider> getAsyncStreamProviders() {
		if (this.asyncStreamProviders == null && this.parent != null)
			return this.parent.getAsyncStreamProviders();
		return this.asyncStreamProviders;
	}

	protected Map<Class<?>, MediaTypeMap<SortedKey<ContextResolver>>> getContextResolvers() {
		if (this.contextResolvers == null && this.parent != null)
			return this.parent.getContextResolvers();
		return this.contextResolvers;
	}

	protected Map<Class<?>, StringConverter> getStringConverters() {
		if (this.stringConverters == null && this.parent != null)
			return this.parent.getStringConverters();
		return this.stringConverters;
	}

	public List<ParamConverterProvider> getParamConverterProviders() {
		if (this.paramConverterProviders != null)
			return this.paramConverterProviders;
		final List<ParamConverterProvider> list = new CopyOnWriteArrayList<>();
		for (final SortedKey<ParamConverterProvider> key : this.getSortedParamConverterProviders())
			list.add(key.getObj());
		this.paramConverterProviders = list;
		return list;
	}

	protected Set<ExtSortedKey<ParamConverterProvider>> getSortedParamConverterProviders() {
		if (this.sortedParamConverterProviders == null && this.parent != null)
			return this.parent.getSortedParamConverterProviders();
		return this.sortedParamConverterProviders;
	}

	protected Map<Class<?>, Class<? extends StringParameterUnmarshaller>> getStringParameterUnmarshallers() {
		if (this.stringParameterUnmarshallers == null && this.parent != null)
			return this.parent.getStringParameterUnmarshallers();
		return this.stringParameterUnmarshallers;
	}

	/**
	 * Copy
	 *
	 * @return
	 */
	public Set<Class<?>> getProviderClasses() {
		if (this.providerClasses == null && this.parent != null)
			return this.parent.getProviderClasses();
		final Set<Class<?>> set = new HashSet<>();
		if (this.parent != null)
			set.addAll(this.parent.getProviderClasses());
		set.addAll(this.providerClasses);
		return set;
	}

	/**
	 * Copy
	 *
	 * @return
	 */
	public Set<Object> getProviderInstances() {
		if (this.providerInstances == null && this.parent != null)
			return this.parent.getProviderInstances();
		final Set<Object> set = new HashSet<>();
		if (this.parent != null)
			set.addAll(this.parent.getProviderInstances());
		set.addAll(this.providerInstances);
		return set;
	}

	public Map<Class<?>, Map<Class<?>, Integer>> getClassContracts() {
		if (this.classContracts != null)
			return this.classContracts;
		final Map<Class<?>, Map<Class<?>, Integer>> map = new ConcurrentHashMap<>();
		if (this.parent != null)
			for (final Map.Entry<Class<?>, Map<Class<?>, Integer>> entry : this.parent.getClassContracts().entrySet()) {
				final Map<Class<?>, Integer> mapEntry = new HashMap<>();
				mapEntry.putAll(entry.getValue());
				map.put(entry.getKey(), mapEntry);
			}
		this.classContracts = map;
		return this.classContracts;
	}

	protected LegacyPrecedence getPrecedence() {
		if (this.precedence == null && this.parent != null)
			return this.parent.getPrecedence();
		return this.precedence;
	}

	public ResteasyProviderFactory getParent() {
		return this.parent;
	}

	protected void registerDefaultInterceptorPrecedences(final InterceptorRegistry registry) {
		// legacy
		registry.appendPrecedence(SecurityPrecedence.PRECEDENCE_STRING);
		registry.appendPrecedence(HeaderDecoratorPrecedence.PRECEDENCE_STRING);
		registry.appendPrecedence(EncoderPrecedence.PRECEDENCE_STRING);
		registry.appendPrecedence(RedirectPrecedence.PRECEDENCE_STRING);
		registry.appendPrecedence(DecoderPrecedence.PRECEDENCE_STRING);

	}

	protected void registerDefaultInterceptorPrecedences() {
		this.precedence.addPrecedence(SecurityPrecedence.PRECEDENCE_STRING, Priorities.AUTHENTICATION);
		this.precedence.addPrecedence(HeaderDecoratorPrecedence.PRECEDENCE_STRING, Priorities.HEADER_DECORATOR);
		this.precedence.addPrecedence(EncoderPrecedence.PRECEDENCE_STRING, Priorities.ENTITY_CODER);
		this.precedence.addPrecedence(RedirectPrecedence.PRECEDENCE_STRING, Priorities.ENTITY_CODER + 50);
		this.precedence.addPrecedence(DecoderPrecedence.PRECEDENCE_STRING, Priorities.ENTITY_CODER);

		this.registerDefaultInterceptorPrecedences(this.getClientExecutionInterceptorRegistry());
	}

	/**
	 * Append interceptor predence
	 *
	 * @param precedence
	 */
	@Deprecated
	public void appendInterceptorPrecedence(final String precedence) {
		if (this.precedence == null)
			this.precedence = this.parent.getPrecedence().clone();
		if (this.clientExecutionInterceptorRegistry == null)
			this.clientExecutionInterceptorRegistry = this.parent.getClientExecutionInterceptorRegistry().cloneTo(this);
		this.precedence.appendPrecedence(precedence);
		this.clientExecutionInterceptorRegistry.appendPrecedence(precedence);
	}

	/**
	 * @param after
	 *            put newPrecedence after this
	 * @param newPrecedence
	 */
	@Deprecated
	public void insertInterceptorPrecedenceAfter(final String after, final String newPrecedence) {
		if (this.precedence == null)
			this.precedence = this.parent.getPrecedence().clone();
		if (this.clientExecutionInterceptorRegistry == null)
			this.clientExecutionInterceptorRegistry = this.parent.getClientExecutionInterceptorRegistry().cloneTo(this);
		this.precedence.insertPrecedenceAfter(after, newPrecedence);

		this.getClientExecutionInterceptorRegistry().insertPrecedenceAfter(after, newPrecedence);
	}

	/**
	 * @param before
	 *            put newPrecedence before this
	 * @param newPrecedence
	 */
	@Deprecated
	public void insertInterceptorPrecedenceBefore(final String before, final String newPrecedence) {
		if (this.precedence == null)
			this.precedence = this.parent.getPrecedence().clone();
		if (this.clientExecutionInterceptorRegistry == null)
			this.clientExecutionInterceptorRegistry = this.parent.getClientExecutionInterceptorRegistry().cloneTo(this);
		this.precedence.insertPrecedenceBefore(before, newPrecedence);

		this.getClientExecutionInterceptorRegistry().insertPrecedenceBefore(before, newPrecedence);
	}

	public static <T> void pushContext(final Class<T> type, final T data) {
		getContextDataMap().put(type, data);
	}

	public static void pushContextDataMap(final Map<Class<?>, Object> map) {
		contextualData.push(map);
	}

	public static Map<Class<?>, Object> getContextDataMap() {
		return getContextDataMap(true);
	}

	public static <T> T getContextData(final Class<T> type) {
		return (T) getContextDataMap().get(type);
	}

	public static <T> T popContextData(final Class<T> type) {
		return (T) getContextDataMap().remove(type);
	}

	public static void clearContextData() {
		contextualData.clear();
	}

	private static Map<Class<?>, Object> getContextDataMap(final boolean create) {
		Map<Class<?>, Object> map = contextualData.get();
		if (map == null)
			contextualData.setLast(map = new HashMap<>());
		return map;
	}

	public static Map<Class<?>, Object> addContextDataLevel() {
		if (getContextDataLevelCount() == maxForwards)
			throw new BadRequestException(
					Messages.MESSAGES.excededMaximumForwards(getContextData(UriInfo.class).getPath()));
		final Map<Class<?>, Object> map = new HashMap<>();
		contextualData.push(map);
		return map;
	}

	public static int getContextDataLevelCount() {
		return contextualData.size();
	}

	public static void removeContextDataLevel() {
		contextualData.pop();
	}

	/**
	 * Will not initialize singleton if not set
	 *
	 * @return
	 */
	public static ResteasyProviderFactory peekInstance() {
		return instance;
	}

	public synchronized static void clearInstanceIfEqual(final ResteasyProviderFactory factory) {
		if (instance == factory) {
			instance = null;
			RuntimeDelegate.setInstance(null);
		}
	}

	public synchronized static void setInstance(final ResteasyProviderFactory factory) {
		synchronized (RD_LOCK) {
			instance = factory;
		}
		RuntimeDelegate.setInstance(factory);
	}

	final static Object RD_LOCK = new Object();

	/**
	 * Initializes ResteasyProviderFactory singleton if not set
	 *
	 * @return
	 */
	public static ResteasyProviderFactory getInstance() {
		ResteasyProviderFactory result = instance;
		if (result == null)
			synchronized (RD_LOCK) {
				result = instance;
				if (result == null) { // Second check (with locking)
					final RuntimeDelegate runtimeDelegate = RuntimeDelegate.getInstance();
					if (runtimeDelegate instanceof ResteasyProviderFactory)
						instance = result = (ResteasyProviderFactory) runtimeDelegate;
					else
						instance = result = new ResteasyProviderFactory();
					if (registerBuiltinByDefault)
						RegisterBuiltin.register(instance);
				}
			}
		return instance;
	}

	public static void setRegisterBuiltinByDefault(final boolean registerBuiltinByDefault) {
		ResteasyProviderFactory.registerBuiltinByDefault = registerBuiltinByDefault;
	}

	public boolean isRegisterBuiltins() {
		return this.registerBuiltins;
	}

	public void setRegisterBuiltins(final boolean registerBuiltins) {
		this.registerBuiltins = registerBuiltins;
	}

	public InjectorFactory getInjectorFactory() {
		if (this.injectorFactory == null && this.parent != null)
			return this.parent.getInjectorFactory();
		return this.injectorFactory;
	}

	public void setInjectorFactory(final InjectorFactory injectorFactory) {
		this.injectorFactory = injectorFactory;
	}

	public InterceptorRegistry<ClientExecutionInterceptor> getClientExecutionInterceptorRegistry() {
		if (this.clientExecutionInterceptorRegistry == null && this.parent != null)
			return this.parent.getClientExecutionInterceptorRegistry();
		return this.clientExecutionInterceptorRegistry;
	}

	public ReaderInterceptorRegistry getServerReaderInterceptorRegistry() {
		if (this.serverReaderInterceptorRegistry == null && this.parent != null)
			return this.parent.getServerReaderInterceptorRegistry();
		return this.serverReaderInterceptorRegistry;
	}

	public WriterInterceptorRegistry getServerWriterInterceptorRegistry() {
		if (this.serverWriterInterceptorRegistry == null && this.parent != null)
			return this.parent.getServerWriterInterceptorRegistry();
		return this.serverWriterInterceptorRegistry;
	}

	public ContainerRequestFilterRegistry getContainerRequestFilterRegistry() {
		if (this.containerRequestFilterRegistry == null && this.parent != null)
			return this.parent.getContainerRequestFilterRegistry();
		return this.containerRequestFilterRegistry;
	}

	public ContainerResponseFilterRegistry getContainerResponseFilterRegistry() {
		if (this.containerResponseFilterRegistry == null && this.parent != null)
			return this.parent.getContainerResponseFilterRegistry();
		return this.containerResponseFilterRegistry;
	}

	public ReaderInterceptorRegistry getClientReaderInterceptorRegistry() {
		if (this.clientReaderInterceptorRegistry == null && this.parent != null)
			return this.parent.getClientReaderInterceptorRegistry();
		return this.clientReaderInterceptorRegistry;
	}

	public WriterInterceptorRegistry getClientWriterInterceptorRegistry() {
		if (this.clientWriterInterceptorRegistry == null && this.parent != null)
			return this.parent.getClientWriterInterceptorRegistry();
		return this.clientWriterInterceptorRegistry;
	}

	public ClientRequestFilterRegistry getClientRequestFilterRegistry() {
		if (this.clientRequestFilterRegistry == null && this.parent != null)
			return this.parent.getClientRequestFilterRegistry();
		return this.clientRequestFilterRegistry;
	}

	/**
	 * This method retained for jaxrs-legacy code. This method is deprecated and is
	 * replace by method, getClientRequestFilterRegistry().
	 *
	 * @return
	 */
	@Deprecated
	public JaxrsInterceptorRegistry<ClientRequestFilter> getClientRequestFilters() {
		if (this.clientRequestFilters == null && this.parent != null)
			return this.parent.getClientRequestFilters();
		return this.clientRequestFilters;
	}

	public ClientResponseFilterRegistry getClientResponseFilters() {
		if (this.clientResponseFilters == null && this.parent != null)
			return this.parent.getClientResponseFilters();
		return this.clientResponseFilters;
	}

	public boolean isBuiltinsRegistered() {
		return this.builtinsRegistered;
	}

	public void setBuiltinsRegistered(final boolean builtinsRegistered) {
		this.builtinsRegistered = builtinsRegistered;
	}

	@Override
	public UriBuilder createUriBuilder() {
		return new ResteasyUriBuilder();
	}

	@Override
	public Response.ResponseBuilder createResponseBuilder() {
		return new ResponseBuilderImpl();
	}

	@Override
	public Variant.VariantListBuilder createVariantListBuilder() {
		return new VariantListBuilderImpl();
	}

	@Override
	public <T> HeaderDelegate<T> createHeaderDelegate(final Class<T> tClass) {
		if (tClass == null)
			throw new IllegalArgumentException(Messages.MESSAGES.tClassParameterNull());
		if (this.headerDelegates == null && this.parent != null)
			return this.parent.createHeaderDelegate(tClass);

		Class<?> clazz = tClass;
		while (clazz != null) {
			HeaderDelegate<T> delegate = this.headerDelegates.get(clazz);
			if (delegate != null)
				return delegate;
			delegate = this.createHeaderDelegateFromInterfaces(clazz.getInterfaces());
			if (delegate != null)
				return delegate;
			clazz = clazz.getSuperclass();
		}

		return this.createHeaderDelegateFromInterfaces(tClass.getInterfaces());
	}

	protected <T> HeaderDelegate<T> createHeaderDelegateFromInterfaces(final Class<?>[] interfaces) {
		HeaderDelegate<T> delegate = null;
		for (final Class<?> interface1 : interfaces) {
			delegate = this.headerDelegates.get(interface1);
			if (delegate != null)
				return delegate;
			delegate = this.createHeaderDelegateFromInterfaces(interface1.getInterfaces());
			if (delegate != null)
				return delegate;
		}
		return null;
	}

	protected Map<Class<?>, HeaderDelegate> getHeaderDelegates() {
		if (this.headerDelegates == null && this.parent != null)
			return this.parent.getHeaderDelegates();
		return this.headerDelegates;
	}

	public void addHeaderDelegate(final Class clazz, final HeaderDelegate header) {
		if (this.headerDelegates == null) {
			this.headerDelegates = new ConcurrentHashMap<>();
			this.headerDelegates.putAll(this.parent.getHeaderDelegates());
		}
		this.headerDelegates.put(clazz, header);
	}

	protected void addMessageBodyReader(final Class<? extends MessageBodyReader> provider, final int priority,
			final boolean isBuiltin) {
		final MessageBodyReader reader = this.createProviderInstance(provider);
		this.addMessageBodyReader(reader, provider, priority, isBuiltin);
	}

	protected void addMessageBodyReader(final MessageBodyReader provider) {
		this.addMessageBodyReader(provider, Priorities.USER, false);
	}

	protected void addMessageBodyReader(final MessageBodyReader provider, final int priority, final boolean isBuiltin) {
		this.addMessageBodyReader(provider, provider.getClass(), priority, isBuiltin);
	}

	/**
	 * Specify the provider class. This is there jsut in case the provider instance
	 * is a proxy. Proxies tend to lose generic type information
	 *
	 * @param provider
	 * @param providerClass
	 * @param isBuiltin
	 */

	protected void addMessageBodyReader(final MessageBodyReader provider, final Class<?> providerClass,
			final int priority, final boolean isBuiltin) {
		final SortedKey<MessageBodyReader> key = new SortedKey<>(MessageBodyReader.class, provider, providerClass,
				priority, isBuiltin);
		this.injectProperties(providerClass, provider);
		final Consumes consumeMime = provider.getClass().getAnnotation(Consumes.class);
		RuntimeType type = null;
		final ConstrainedTo constrainedTo = providerClass.getAnnotation(ConstrainedTo.class);
		if (constrainedTo != null)
			type = constrainedTo.value();

		if (type == null) {
			this.addClientMessageBodyReader(key, consumeMime);
			this.addServerMessageBodyReader(key, consumeMime);
		} else if (type == RuntimeType.CLIENT)
			this.addClientMessageBodyReader(key, consumeMime);
		else
			this.addServerMessageBodyReader(key, consumeMime);
	}

	protected void addServerMessageBodyReader(final SortedKey<MessageBodyReader> key, final Consumes consumeMime) {
		if (this.serverMessageBodyReaders == null)
			this.serverMessageBodyReaders = this.parent.getServerMessageBodyReaders().clone();
		if (consumeMime != null)
			for (final String consume : consumeMime.value()) {
				final MediaType mime = MediaType.valueOf(consume);
				this.serverMessageBodyReaders.add(mime, key);
			}
		else
			this.serverMessageBodyReaders.add(new MediaType("*", "*"), key);
	}

	protected void addClientMessageBodyReader(final SortedKey<MessageBodyReader> key, final Consumes consumeMime) {
		if (this.clientMessageBodyReaders == null)
			this.clientMessageBodyReaders = this.parent.getClientMessageBodyReaders().clone();
		if (consumeMime != null)
			for (final String consume : consumeMime.value()) {
				final MediaType mime = MediaType.valueOf(consume);
				this.clientMessageBodyReaders.add(mime, key);
			}
		else
			this.clientMessageBodyReaders.add(new MediaType("*", "*"), key);
	}

	protected void addMessageBodyWriter(final Class<? extends MessageBodyWriter> provider, final int priority,
			final boolean isBuiltin) {
		final MessageBodyWriter writer = this.createProviderInstance(provider);
		this.addMessageBodyWriter(writer, provider, priority, isBuiltin);
	}

	protected void addMessageBodyWriter(final MessageBodyWriter provider) {
		this.addMessageBodyWriter(provider, provider.getClass(), Priorities.USER, false);
	}

	/**
	 * Specify the provider class. This is there jsut in case the provider instance
	 * is a proxy. Proxies tend to lose generic type information
	 *
	 * @param provider
	 * @param providerClass
	 * @param isBuiltin
	 */
	protected void addMessageBodyWriter(final MessageBodyWriter provider, final Class<?> providerClass,
			final int priority, final boolean isBuiltin) {
		this.injectProperties(providerClass, provider);
		final Produces consumeMime = provider.getClass().getAnnotation(Produces.class);
		final SortedKey<MessageBodyWriter> key = new SortedKey<>(MessageBodyWriter.class, provider, providerClass,
				priority, isBuiltin);
		RuntimeType type = null;
		final ConstrainedTo constrainedTo = providerClass.getAnnotation(ConstrainedTo.class);
		if (constrainedTo != null)
			type = constrainedTo.value();
		if (type == null) {
			this.addClientMessageBodyWriter(consumeMime, key);
			this.addServerMessageBodyWriter(consumeMime, key);

		} else if (type == RuntimeType.CLIENT)
			this.addClientMessageBodyWriter(consumeMime, key);
		else
			this.addServerMessageBodyWriter(consumeMime, key);
	}

	protected void addServerMessageBodyWriter(final Produces consumeMime, final SortedKey<MessageBodyWriter> key) {
		if (this.serverMessageBodyWriters == null)
			this.serverMessageBodyWriters = this.parent.getServerMessageBodyWriters().clone();
		if (consumeMime != null)
			for (final String consume : consumeMime.value()) {
				final MediaType mime = MediaType.valueOf(consume);
				// logger.info(">>> Adding provider: " + provider.getClass().getName() + " with
				// mime type of: " + mime);
				this.serverMessageBodyWriters.add(mime, key);
			}
		else
			// logger.info(">>> Adding provider: " + provider.getClass().getName() + " with
			// mime type of: default */*");
			this.serverMessageBodyWriters.add(new MediaType("*", "*"), key);
	}

	protected void addClientMessageBodyWriter(final Produces consumeMime, final SortedKey<MessageBodyWriter> key) {
		if (this.clientMessageBodyWriters == null)
			this.clientMessageBodyWriters = this.parent.getClientMessageBodyWriters().clone();
		if (consumeMime != null)
			for (final String consume : consumeMime.value()) {
				final MediaType mime = MediaType.valueOf(consume);
				// logger.info(">>> Adding provider: " + provider.getClass().getName() + " with
				// mime type of: " + mime);
				this.clientMessageBodyWriters.add(mime, key);
			}
		else
			// logger.info(">>> Adding provider: " + provider.getClass().getName() + " with
			// mime type of: default */*");
			this.clientMessageBodyWriters.add(new MediaType("*", "*"), key);
	}

	public <T> MessageBodyReader<T> getServerMessageBodyReader(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		final MediaTypeMap<SortedKey<MessageBodyReader>> availableReaders = this.getServerMessageBodyReaders();
		return this.resolveMessageBodyReader(type, genericType, annotations, mediaType, availableReaders);
	}

	/**
	 * Always returns server MBRs
	 *
	 * @param type
	 *            the class of the object that is to be read.
	 * @param genericType
	 *            the type of object to be produced. E.g. if the message body is to
	 *            be converted into a method parameter, this will be the formal type
	 *            of the method parameter as returned by
	 *            {@code Class.getGenericParameterTypes}.
	 * @param annotations
	 *            an array of the annotations on the declaration of the artifact
	 *            that will be initialized with the produced instance. E.g. if the
	 *            message body is to be converted into a method parameter, this will
	 *            be the annotations on that parameter returned by
	 *            {@code Class.getParameterAnnotations}.
	 * @param mediaType
	 *            the media type of the data that will be read.
	 * @param <T>
	 * @return
	 */
	@Override
	public <T> MessageBodyReader<T> getMessageBodyReader(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		final MediaTypeMap<SortedKey<MessageBodyReader>> availableReaders = this.getServerMessageBodyReaders();
		final MessageBodyReader<T> reader = this.resolveMessageBodyReader(type, genericType, annotations, mediaType,
				availableReaders);
		if (reader != null)
			LogMessages.LOGGER.debugf("MessageBodyReader: %s", reader.getClass().getName());
		return reader;
	}

	public <T> MessageBodyReader<T> getClientMessageBodyReader(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		final MediaTypeMap<SortedKey<MessageBodyReader>> availableReaders = this.getClientMessageBodyReaders();
		return this.resolveMessageBodyReader(type, genericType, annotations, mediaType, availableReaders);
	}

	protected <T> MessageBodyReader<T> resolveMessageBodyReader(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType,
			final MediaTypeMap<SortedKey<MessageBodyReader>> availableReaders) {
		final List<SortedKey<MessageBodyReader>> readers = availableReaders.getPossible(mediaType, type);

		// logger.info("******** getMessageBodyReader *******");
		for (final SortedKey<MessageBodyReader> reader : readers)
			// logger.info(" matching reader: " + reader.getClass().getName());
			if (reader.obj.isReadable(type, genericType, annotations, mediaType)) {
				LogMessages.LOGGER.debugf("MessageBodyReader: %s", reader.getClass().getName());
				return reader.obj;
			}
		return null;
	}

	protected void addExceptionMapper(final Class<? extends ExceptionMapper> providerClass) {
		this.addExceptionMapper(providerClass, false);
	}

	protected void addExceptionMapper(final ExceptionMapper provider) {
		this.addExceptionMapper(provider, false);
	}

	protected void addExceptionMapper(final ExceptionMapper provider, final Class providerClass) {
		this.addExceptionMapper(provider, providerClass, false);
	}

	protected void addExceptionMapper(final ExceptionMapper provider, final Type exceptionType) {
		this.addExceptionMapper(provider, exceptionType, provider.getClass(), false);
	}

	protected void addExceptionMapper(final Class<? extends ExceptionMapper> providerClass, final boolean isBuiltin) {
		final ExceptionMapper provider = this.createProviderInstance(providerClass);
		this.addExceptionMapper(provider, providerClass, isBuiltin);
	}

	protected void addExceptionMapper(final ExceptionMapper provider, final boolean isBuiltin) {
		this.addExceptionMapper(provider, provider.getClass(), isBuiltin);
	}

	protected void addExceptionMapper(final ExceptionMapper provider, Class providerClass, final boolean isBuiltin) {
		// Check for weld proxy.
		if (providerClass.isSynthetic())
			providerClass = providerClass.getSuperclass();
		final Type exceptionType = Types.getActualTypeArgumentsOfAnInterface(providerClass, ExceptionMapper.class)[0];
		this.addExceptionMapper(provider, exceptionType, providerClass, isBuiltin);
	}

	protected void addExceptionMapper(final ExceptionMapper provider, final Type exceptionType, Class providerClass,
			final boolean isBuiltin) {
		// Check for weld proxy.
		if (providerClass.isSynthetic())
			providerClass = providerClass.getSuperclass();
		this.injectProperties(providerClass, provider);

		final Class<?> exceptionClass = Types.getRawType(exceptionType);
		if (!Throwable.class.isAssignableFrom(exceptionClass))
			throw new RuntimeException(Messages.MESSAGES.incorrectTypeParameterExceptionMapper());
		if (this.sortedExceptionMappers == null) {
			this.sortedExceptionMappers = new ConcurrentHashMap<>();
			this.sortedExceptionMappers.putAll(this.parent.getSortedExceptionMappers());
		}
		final int priority = this.getPriority(null, null, ExceptionMapper.class, providerClass);
		final SortedKey<ExceptionMapper> candidateExceptionMapper = new SortedKey<>(null, provider, providerClass,
				priority, isBuiltin);
		SortedKey<ExceptionMapper> registeredExceptionMapper;
		if ((registeredExceptionMapper = this.sortedExceptionMappers.get(exceptionClass)) != null
				&& candidateExceptionMapper.compareTo(registeredExceptionMapper) > 0)
			return;
		this.sortedExceptionMappers.put(exceptionClass, candidateExceptionMapper);
		this.exceptionMappers = null;
	}

	public void addClientExceptionMapper(final Class<? extends ClientExceptionMapper<?>> providerClass) {
		final ClientExceptionMapper<?> provider = this.createProviderInstance(providerClass);
		this.addClientExceptionMapper(provider, providerClass);
	}

	public void addClientExceptionMapper(final ClientExceptionMapper<?> provider) {
		this.addClientExceptionMapper(provider, provider.getClass());
	}

	public void addClientExceptionMapper(final ClientExceptionMapper<?> provider, final Class<?> providerClass) {
		final Type exceptionType = Types.getActualTypeArgumentsOfAnInterface(providerClass,
				ClientExceptionMapper.class)[0];
		this.addClientExceptionMapper(provider, exceptionType);
	}

	public void addClientExceptionMapper(final ClientExceptionMapper<?> provider, final Type exceptionType) {
		this.injectProperties(provider.getClass());

		final Class<?> exceptionClass = Types.getRawType(exceptionType);
		if (!Throwable.class.isAssignableFrom(exceptionClass))
			throw new RuntimeException(Messages.MESSAGES.incorrectTypeParameterClientExceptionMapper());
		if (this.clientExceptionMappers == null) {
			this.clientExceptionMappers = new ConcurrentHashMap<>();
			this.clientExceptionMappers.putAll(this.parent.getClientExceptionMappers());
		}
		this.clientExceptionMappers.put(exceptionClass, provider);
	}

	/**
	 * Add a {@link ClientErrorInterceptor} to this provider factory instance.
	 * Duplicate handlers are ignored. (For Client Proxy API only)
	 */
	public void addClientErrorInterceptor(final ClientErrorInterceptor handler) {
		if (this.clientErrorInterceptors == null)
			this.clientErrorInterceptors = new CopyOnWriteArrayList<>(this.parent.getClientErrorInterceptors());
		if (!this.clientErrorInterceptors.contains(handler))
			this.clientErrorInterceptors.add(handler);
	}

	/**
	 * Return the list of currently registered {@link ClientErrorInterceptor}
	 * instances.
	 */
	public List<ClientErrorInterceptor> getClientErrorInterceptors() {
		if (this.clientErrorInterceptors == null && this.parent != null)
			return this.parent.getClientErrorInterceptors();
		return this.clientErrorInterceptors;
	}

	protected void addAsyncResponseProvider(final Class<? extends AsyncResponseProvider> providerClass) {
		final AsyncResponseProvider provider = this.createProviderInstance(providerClass);
		this.addAsyncResponseProvider(provider, providerClass);
	}

	protected void addAsyncResponseProvider(final AsyncResponseProvider provider) {
		this.addAsyncResponseProvider(provider, provider.getClass());
	}

	protected void addAsyncResponseProvider(final AsyncResponseProvider provider, final Class providerClass) {
		final Type asyncType = Types.getActualTypeArgumentsOfAnInterface(providerClass, AsyncResponseProvider.class)[0];
		this.addAsyncResponseProvider(provider, asyncType);
	}

	protected void addAsyncResponseProvider(final AsyncResponseProvider provider, final Type asyncType) {
		this.injectProperties(provider.getClass(), provider);

		final Class<?> asyncClass = Types.getRawType(asyncType);
		if (this.asyncResponseProviders == null) {
			this.asyncResponseProviders = new ConcurrentHashMap<>();
			this.asyncResponseProviders.putAll(this.parent.getAsyncResponseProviders());
		}
		this.asyncResponseProviders.put(asyncClass, provider);
	}

	protected void addAsyncStreamProvider(final Class<? extends AsyncStreamProvider> providerClass) {
		final AsyncStreamProvider provider = this.createProviderInstance(providerClass);
		this.addAsyncStreamProvider(provider, providerClass);
	}

	protected void addAsyncStreamProvider(final AsyncStreamProvider provider) {
		this.addAsyncStreamProvider(provider, provider.getClass());
	}

	protected void addAsyncStreamProvider(final AsyncStreamProvider provider, final Class providerClass) {
		final Type asyncType = Types.getActualTypeArgumentsOfAnInterface(providerClass, AsyncStreamProvider.class)[0];
		this.addAsyncStreamProvider(provider, asyncType);
	}

	protected void addAsyncStreamProvider(final AsyncStreamProvider provider, final Type asyncType) {
		this.injectProperties(provider.getClass(), provider);

		final Class<?> asyncClass = Types.getRawType(asyncType);
		if (this.asyncStreamProviders == null) {
			this.asyncStreamProviders = new ConcurrentHashMap<>();
			this.asyncStreamProviders.putAll(this.parent.getAsyncStreamProviders());
		}
		this.asyncStreamProviders.put(asyncClass, provider);
	}

	protected void addContextResolver(final Class<? extends ContextResolver> resolver, final boolean builtin) {
		final ContextResolver writer = this.createProviderInstance(resolver);
		this.addContextResolver(writer, resolver, builtin);
	}

	protected void addContextResolver(final ContextResolver provider) {
		this.addContextResolver(provider, false);
	}

	protected void addContextResolver(final ContextResolver provider, final boolean builtin) {
		this.addContextResolver(provider, provider.getClass(), builtin);
	}

	protected void addContextResolver(final ContextResolver provider, final Class providerClass,
			final boolean builtin) {
		final Type parameter = Types.getActualTypeArgumentsOfAnInterface(providerClass, ContextResolver.class)[0];
		this.addContextResolver(provider, parameter, providerClass, builtin);
	}

	protected void addContextResolver(final ContextResolver provider, final Type typeParameter,
			final Class providerClass, final boolean builtin) {
		this.injectProperties(providerClass, provider);
		final Class<?> parameterClass = Types.getRawType(typeParameter);
		if (this.contextResolvers == null) {
			this.contextResolvers = new ConcurrentHashMap<>();
			for (final Map.Entry<Class<?>, MediaTypeMap<SortedKey<ContextResolver>>> entry : this.parent
					.getContextResolvers().entrySet())
				this.contextResolvers.put(entry.getKey(), entry.getValue().clone());
		}
		MediaTypeMap<SortedKey<ContextResolver>> resolvers = this.contextResolvers.get(parameterClass);
		if (resolvers == null) {
			resolvers = new MediaTypeMap<>();
			this.contextResolvers.put(parameterClass, resolvers);
		}
		final Produces produces = provider.getClass().getAnnotation(Produces.class);
		final int priority = this.getPriority(null, null, ContextResolver.class, providerClass);
		final SortedKey<ContextResolver> key = new SortedKey<>(ContextResolver.class, provider, providerClass, priority,
				builtin);
		if (produces != null)
			for (final String produce : produces.value()) {
				final MediaType mime = MediaType.valueOf(produce);
				resolvers.add(mime, key);
			}
		else
			resolvers.add(new MediaType("*", "*"), key);
	}

	protected void addStringConverter(final Class<? extends StringConverter> resolver) {
		final StringConverter writer = this.createProviderInstance(resolver);
		this.addStringConverter(writer, resolver);
	}

	protected void addStringConverter(final StringConverter provider) {
		this.addStringConverter(provider, provider.getClass());
	}

	protected void addStringConverter(final StringConverter provider, final Class providerClass) {
		final Type parameter = Types.getActualTypeArgumentsOfAnInterface(providerClass, StringConverter.class)[0];
		this.addStringConverter(provider, parameter);
	}

	protected void addStringConverter(final StringConverter provider, final Type typeParameter) {
		this.injectProperties(provider.getClass(), provider);
		final Class<?> parameterClass = Types.getRawType(typeParameter);
		if (this.stringConverters == null) {
			this.stringConverters = new ConcurrentHashMap<>();
			this.stringConverters.putAll(this.parent.getStringConverters());
		}
		this.stringConverters.put(parameterClass, provider);
	}

	public void addStringParameterUnmarshaller(final Class<? extends StringParameterUnmarshaller> provider) {
		if (this.stringParameterUnmarshallers == null) {
			this.stringParameterUnmarshallers = new ConcurrentHashMap<>();
			this.stringParameterUnmarshallers.putAll(this.parent.getStringParameterUnmarshallers());
		}
		final Type[] intfs = provider.getGenericInterfaces();
		for (final Type type : intfs)
			if (type instanceof ParameterizedType) {
				final ParameterizedType pt = (ParameterizedType) type;
				if (pt.getRawType().equals(StringParameterUnmarshaller.class)) {
					final Class<?> aClass = Types.getRawType(pt.getActualTypeArguments()[0]);
					this.stringParameterUnmarshallers.put(aClass, provider);
				}
			}
	}

	public List<ContextResolver> getContextResolvers(final Class<?> clazz, final MediaType type) {
		final MediaTypeMap<SortedKey<ContextResolver>> resolvers = this.getContextResolvers().get(clazz);
		if (resolvers == null)
			return null;
		final List<ContextResolver> rtn = new ArrayList<>();

		final List<SortedKey<ContextResolver>> list = resolvers.getPossible(type);
		if (type.isWildcardType()) {
			// do it upside down if it is a wildcard type: Note: this is to pass the stupid
			// TCK which prefers that
			// a wildcard type match up with other wildcard types
			// for (int i = list.size() - 1; i >= 0; i--)
			// {
			// rtn.add(list.get(i).obj);
			// }

			// Fix for RESTEASY-1609.
			// This is related to the fix in RESTEASY-1471, prior to which user
			// ContextResolvers appeared
			// to be built-in. The original loop may have been in response to that bug, so
			// the reversal
			// may not be necessary. In any case, this code will do the reversal but put
			// user ContextResolvers
			// at the beginning of the list.
			for (int i = list.size() - 1; i >= 0; i--)
				if (!list.get(i).isBuiltin)
					rtn.add(list.get(i).obj);
			for (int i = list.size() - 1; i >= 0; i--)
				if (list.get(i).isBuiltin)
					rtn.add(list.get(i).obj);
		} else
			for (final SortedKey<ContextResolver> resolver : list)
				rtn.add(resolver.obj);
		return rtn;
	}

	public ParamConverter getParamConverter(final Class clazz, final Type genericType, final Annotation[] annotations) {
		for (final SortedKey<ParamConverterProvider> provider : this.getSortedParamConverterProviders()) {
			final ParamConverter converter = provider.getObj().getConverter(clazz, genericType, annotations);
			if (converter != null)
				return converter;
		}
		return null;
	}

	public StringConverter getStringConverter(final Class<?> clazz) {
		if (this.getStringConverters().size() == 0)
			return null;
		return this.getStringConverters().get(clazz);
	}

	public <T> StringParameterUnmarshaller<T> createStringParameterUnmarshaller(final Class<T> clazz) {
		if (this.getStringParameterUnmarshallers().size() == 0)
			return null;
		final Class<? extends StringParameterUnmarshaller> un = this.getStringParameterUnmarshallers().get(clazz);
		if (un == null)
			return null;
		final StringParameterUnmarshaller<T> provider = this.injectedInstance(un);
		return provider;

	}

	public void registerProvider(final Class provider) {
		this.registerProvider(provider, false);
	}

	/**
	 * Convert an object to a string. First try StringConverter then,
	 * object.ToString()
	 *
	 * @param object
	 * @return
	 */
	public String toString(final Object object, final Class clazz, final Type genericType,
			final Annotation[] annotations) {
		if (object instanceof String)
			return (String) object;
		final ParamConverter paramConverter = this.getParamConverter(clazz, genericType, annotations);
		if (paramConverter != null)
			return paramConverter.toString(object);
		final StringConverter converter = this.getStringConverter(object.getClass());
		if (converter != null)
			return converter.toString(object);
		else
			return object.toString();

	}

	@Override
	public String toHeaderString(final Object object) {
		if (object == null)
			return "";
		if (object instanceof String)
			return (String) object;
		final Class<?> aClass = object.getClass();
		final ParamConverter paramConverter = this.getParamConverter(aClass, null, null);
		if (paramConverter != null)
			return paramConverter.toString(object);
		final StringConverter converter = this.getStringConverter(aClass);
		if (converter != null)
			return converter.toString(object);

		final HeaderDelegate delegate = this.getHeaderDelegate(aClass);
		if (delegate != null)
			return delegate.toString(object);
		else
			return object.toString();

	}

	/**
	 * Checks to see if RuntimeDelegate is a ResteasyProviderFactory If it is, then
	 * use that, otherwise use this
	 *
	 * @param aClass
	 * @return
	 */
	public HeaderDelegate getHeaderDelegate(final Class<?> aClass) {
		HeaderDelegate delegate = null;
		// Stupid idiotic TCK calls RuntimeDelegate.setInstance()
		if (RuntimeDelegate.getInstance() instanceof ResteasyProviderFactory)
			delegate = this.createHeaderDelegate(aClass);
		else
			delegate = RuntimeDelegate.getInstance().createHeaderDelegate(aClass);
		return delegate;
	}

	/**
	 * Register a @Provider class. Can be a MessageBodyReader/Writer or
	 * ExceptionMapper.
	 *
	 * @param provider
	 */
	public void registerProvider(final Class provider, final boolean isBuiltin) {
		this.registerProvider(provider, null, isBuiltin, null);
	}

	protected boolean isA(final Class target, final Class type, final Map<Class<?>, Integer> contracts) {
		if (!type.isAssignableFrom(target))
			return false;
		if (contracts == null || contracts.size() == 0)
			return true;
		for (final Class<?> contract : contracts.keySet())
			if (contract.equals(type))
				return true;
		return false;
	}

	protected boolean isA(final Object target, final Class type, final Map<Class<?>, Integer> contracts) {
		return this.isA(target.getClass(), type, contracts);
	}

	protected int getPriority(final Integer override, final Map<Class<?>, Integer> contracts, final Class type,
			Class<?> component) {
		if (override != null)
			return override;
		if (contracts != null) {
			final Integer p = contracts.get(type);
			if (p != null)
				return p;
		}
		// Check for weld proxy.
		component = component.isSynthetic() ? component.getSuperclass() : component;
		final Priority priority = component.getAnnotation(Priority.class);
		if (priority == null)
			return Priorities.USER;
		return priority.value();
	}

	public void registerProvider(final Class provider, final Integer priorityOverride, final boolean isBuiltin,
			final Map<Class<?>, Integer> contracts) {
		if (this.getClasses().contains(provider)) {
			LogMessages.LOGGER.providerClassAlreadyRegistered(provider.getName());
			return;
		}
		for (final Object registered : this.getInstances())
			if (registered.getClass() == provider) {
				LogMessages.LOGGER.providerClassAlreadyRegistered(provider.getName());
				return;
			}
		final Map<Class<?>, Integer> newContracts = new HashMap<>();

		if (this.isA(provider, ParamConverterProvider.class, contracts)) {
			final ParamConverterProvider paramConverterProvider = (ParamConverterProvider) this
					.injectedInstance(provider);
			this.injectProperties(provider);
			if (this.sortedParamConverterProviders == null)
				this.sortedParamConverterProviders = Collections
						.synchronizedSortedSet(new TreeSet<>(this.parent.getSortedParamConverterProviders()));
			final int priority = this.getPriority(priorityOverride, contracts, ParamConverterProvider.class, provider);
			this.sortedParamConverterProviders
					.add(new ExtSortedKey<>(null, paramConverterProvider, provider, priority, isBuiltin));
			this.paramConverterProviders = null;
			newContracts.put(ParamConverterProvider.class, priority);
		}
		if (this.isA(provider, MessageBodyReader.class, contracts))
			try {
				final int priority = this.getPriority(priorityOverride, contracts, MessageBodyReader.class, provider);
				this.addMessageBodyReader(provider, priority, isBuiltin);
				newContracts.put(MessageBodyReader.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateMessageBodyReader(), e);
			}
		if (this.isA(provider, MessageBodyWriter.class, contracts))
			try {
				final int priority = this.getPriority(priorityOverride, contracts, MessageBodyWriter.class, provider);
				this.addMessageBodyWriter(provider, priority, isBuiltin);
				newContracts.put(MessageBodyWriter.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateMessageBodyWriter(), e);
			}
		if (this.isA(provider, ExceptionMapper.class, contracts))
			try {
				this.addExceptionMapper(provider, isBuiltin);
				newContracts.put(ExceptionMapper.class,
						this.getPriority(priorityOverride, contracts, ExceptionMapper.class, provider));
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateExceptionMapper(), e);
			}

		if (this.isA(provider, ClientExceptionMapper.class, contracts))
			try {
				this.addClientExceptionMapper(provider);
				newContracts.put(ClientExceptionMapper.class,
						this.getPriority(priorityOverride, contracts, ClientExceptionMapper.class, provider));
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateClientExceptionMapper(), e);
			}
		if (this.isA(provider, AsyncResponseProvider.class, contracts))
			try {
				this.addAsyncResponseProvider(provider);
				newContracts.put(AsyncResponseProvider.class,
						this.getPriority(priorityOverride, contracts, AsyncResponseProvider.class, provider));
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateAsyncResponseProvider(), e);
			}
		if (this.isA(provider, AsyncStreamProvider.class, contracts))
			try {
				this.addAsyncStreamProvider(provider);
				newContracts.put(AsyncStreamProvider.class,
						this.getPriority(priorityOverride, contracts, AsyncStreamProvider.class, provider));
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateAsyncStreamProvider(), e);
			}
		if (this.isA(provider, ClientRequestFilter.class, contracts)) {
			if (this.clientRequestFilterRegistry == null)
				this.clientRequestFilterRegistry = this.parent.getClientRequestFilterRegistry().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ClientRequestFilter.class, provider);
			this.clientRequestFilterRegistry.registerClass(provider, priority);
			newContracts.put(ClientRequestFilter.class, priority);

			{ // code maintained for backward compatibility for jaxrs-legacy code
				if (this.clientRequestFilters == null)
					this.clientRequestFilters = this.parent.getClientRequestFilters().clone(this);
				this.clientRequestFilters.registerClass(provider, priority);
			}

		}
		if (this.isA(provider, ClientResponseFilter.class, contracts)) {
			if (this.clientResponseFilters == null)
				this.clientResponseFilters = this.parent.getClientResponseFilters().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ClientResponseFilter.class, provider);
			this.clientResponseFilters.registerClass(provider, priority);
			newContracts.put(ClientResponseFilter.class, priority);
		}
		if (this.isA(provider, ClientExecutionInterceptor.class, contracts)) {
			if (this.clientExecutionInterceptorRegistry == null)
				this.clientExecutionInterceptorRegistry = this.parent.getClientExecutionInterceptorRegistry()
						.cloneTo(this);
			this.clientExecutionInterceptorRegistry.register(provider);
			newContracts.put(ClientExecutionInterceptor.class, 0);
		}
		if (this.isA(provider, PreProcessInterceptor.class, contracts)) {
			if (this.containerRequestFilterRegistry == null)
				this.containerRequestFilterRegistry = this.parent.getContainerRequestFilterRegistry().clone(this);
			this.containerRequestFilterRegistry.registerLegacy(provider);
			newContracts.put(PreProcessInterceptor.class, 0);
		}
		if (this.isA(provider, PostProcessInterceptor.class, contracts)) {
			if (this.containerResponseFilterRegistry == null)
				this.containerResponseFilterRegistry = this.parent.getContainerResponseFilterRegistry().clone(this);
			this.containerResponseFilterRegistry.registerLegacy(provider);
			newContracts.put(PostProcessInterceptor.class, 0);
		}
		if (this.isA(provider, ContainerRequestFilter.class, contracts)) {
			if (this.containerRequestFilterRegistry == null)
				this.containerRequestFilterRegistry = this.parent.getContainerRequestFilterRegistry().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ContainerRequestFilter.class, provider);
			this.containerRequestFilterRegistry.registerClass(provider, priority);
			newContracts.put(ContainerRequestFilter.class, priority);
		}
		if (this.isA(provider, ContainerResponseFilter.class, contracts)) {
			if (this.containerResponseFilterRegistry == null)
				this.containerResponseFilterRegistry = this.parent.getContainerResponseFilterRegistry().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ContainerResponseFilter.class, provider);
			this.containerResponseFilterRegistry.registerClass(provider, priority);
			newContracts.put(ContainerResponseFilter.class, priority);
		}
		if (this.isA(provider, ReaderInterceptor.class, contracts)) {
			final ConstrainedTo constrainedTo = (ConstrainedTo) provider.getAnnotation(ConstrainedTo.class);
			final int priority = this.getPriority(priorityOverride, contracts, ReaderInterceptor.class, provider);
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.SERVER) {
				if (this.serverReaderInterceptorRegistry == null)
					this.serverReaderInterceptorRegistry = this.parent.getServerReaderInterceptorRegistry().clone(this);
				this.serverReaderInterceptorRegistry.registerClass(provider, priority);
			}
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.CLIENT) {
				if (this.clientReaderInterceptorRegistry == null)
					this.clientReaderInterceptorRegistry = this.parent.getClientReaderInterceptorRegistry().clone(this);
				this.clientReaderInterceptorRegistry.registerClass(provider, priority);
			}
			if (constrainedTo == null) {
				if (this.serverReaderInterceptorRegistry == null)
					this.serverReaderInterceptorRegistry = this.parent.getServerReaderInterceptorRegistry().clone(this);
				this.serverReaderInterceptorRegistry.registerClass(provider, priority);
				if (this.clientReaderInterceptorRegistry == null)
					this.clientReaderInterceptorRegistry = this.parent.getClientReaderInterceptorRegistry().clone(this);
				this.clientReaderInterceptorRegistry.registerClass(provider, priority);
			}
			newContracts.put(ReaderInterceptor.class, priority);
		}
		if (this.isA(provider, WriterInterceptor.class, contracts)) {
			final ConstrainedTo constrainedTo = (ConstrainedTo) provider.getAnnotation(ConstrainedTo.class);
			final int priority = this.getPriority(priorityOverride, contracts, WriterInterceptor.class, provider);
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.SERVER) {
				if (this.serverWriterInterceptorRegistry == null)
					this.serverWriterInterceptorRegistry = this.parent.getServerWriterInterceptorRegistry().clone(this);
				this.serverWriterInterceptorRegistry.registerClass(provider, priority);
			}
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.CLIENT) {
				if (this.clientWriterInterceptorRegistry == null)
					this.clientWriterInterceptorRegistry = this.parent.getClientWriterInterceptorRegistry().clone(this);
				this.clientWriterInterceptorRegistry.registerClass(provider, priority);
			}
			if (constrainedTo == null) {
				if (this.serverWriterInterceptorRegistry == null)
					this.serverWriterInterceptorRegistry = this.parent.getServerWriterInterceptorRegistry().clone(this);
				this.serverWriterInterceptorRegistry.registerClass(provider, priority);
				if (this.clientWriterInterceptorRegistry == null)
					this.clientWriterInterceptorRegistry = this.parent.getClientWriterInterceptorRegistry().clone(this);
				this.clientWriterInterceptorRegistry.registerClass(provider, priority);
			}
			newContracts.put(WriterInterceptor.class, priority);
		}
		if (this.isA(provider, MessageBodyWriterInterceptor.class, contracts)) {
			if (provider.isAnnotationPresent(ServerInterceptor.class)) {
				if (this.serverWriterInterceptorRegistry == null)
					this.serverWriterInterceptorRegistry = this.parent.getServerWriterInterceptorRegistry().clone(this);
				this.serverWriterInterceptorRegistry.registerLegacy(provider);
			}
			if (provider.isAnnotationPresent(ClientInterceptor.class)) {
				if (this.clientWriterInterceptorRegistry == null)
					this.clientWriterInterceptorRegistry = this.parent.getClientWriterInterceptorRegistry().clone(this);
				this.clientWriterInterceptorRegistry.registerLegacy(provider);
			}
			if (!provider.isAnnotationPresent(ServerInterceptor.class)
					&& !provider.isAnnotationPresent(ClientInterceptor.class))
				throw new RuntimeException(Messages.MESSAGES.interceptorClassMustBeAnnotated());
			newContracts.put(MessageBodyWriterInterceptor.class, 0);

		}
		if (this.isA(provider, MessageBodyReaderInterceptor.class, contracts)) {
			if (provider.isAnnotationPresent(ServerInterceptor.class)) {
				if (this.serverReaderInterceptorRegistry == null)
					this.serverReaderInterceptorRegistry = this.parent.getServerReaderInterceptorRegistry().clone(this);
				this.serverReaderInterceptorRegistry.registerLegacy(provider);
			}
			if (provider.isAnnotationPresent(ClientInterceptor.class)) {
				if (this.clientReaderInterceptorRegistry == null)
					this.clientReaderInterceptorRegistry = this.parent.getClientReaderInterceptorRegistry().clone(this);
				this.clientReaderInterceptorRegistry.registerLegacy(provider);
			}
			if (!provider.isAnnotationPresent(ServerInterceptor.class)
					&& !provider.isAnnotationPresent(ClientInterceptor.class))
				throw new RuntimeException(Messages.MESSAGES.interceptorClassMustBeAnnotated());
			newContracts.put(MessageBodyReaderInterceptor.class, 0);

		}
		if (this.isA(provider, ContextResolver.class, contracts))
			try {
				this.addContextResolver(provider, isBuiltin);
				final int priority = this.getPriority(priorityOverride, contracts, ContextResolver.class, provider);
				newContracts.put(ContextResolver.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateContextResolver(), e);
			}
		if (this.isA(provider, StringConverter.class, contracts)) {
			this.addStringConverter(provider);
			final int priority = this.getPriority(priorityOverride, contracts, StringConverter.class, provider);
			newContracts.put(StringConverter.class, priority);
		}
		if (this.isA(provider, StringParameterUnmarshaller.class, contracts)) {
			this.addStringParameterUnmarshaller(provider);
			final int priority = this.getPriority(priorityOverride, contracts, StringParameterUnmarshaller.class,
					provider);
			newContracts.put(StringParameterUnmarshaller.class, priority);
		}
		if (this.isA(provider, InjectorFactory.class, contracts))
			try {
				this.injectorFactory = (InjectorFactory) provider.newInstance();
				newContracts.put(InjectorFactory.class, 0);
			} catch (final Exception e) {
				throw new RuntimeException(e);
			}
		if (this.isA(provider, DynamicFeature.class, contracts)) {
			final ConstrainedTo constrainedTo = (ConstrainedTo) provider.getAnnotation(ConstrainedTo.class);
			final int priority = this.getPriority(priorityOverride, contracts, DynamicFeature.class, provider);
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.SERVER) {
				if (this.serverDynamicFeatures == null)
					this.serverDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getServerDynamicFeatures());
				this.serverDynamicFeatures.add((DynamicFeature) this.injectedInstance(provider));
			}
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.CLIENT) {
				if (this.clientDynamicFeatures == null)
					this.clientDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getClientDynamicFeatures());
				this.clientDynamicFeatures.add((DynamicFeature) this.injectedInstance(provider));
			}
			if (constrainedTo == null) {
				if (this.serverDynamicFeatures == null)
					this.serverDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getServerDynamicFeatures());
				this.serverDynamicFeatures.add((DynamicFeature) this.injectedInstance(provider));
				if (this.clientDynamicFeatures == null)
					this.clientDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getClientDynamicFeatures());
				this.clientDynamicFeatures.add((DynamicFeature) this.injectedInstance(provider));
			}
			newContracts.put(DynamicFeature.class, priority);
		}
		if (this.isA(provider, Feature.class, contracts)) {
			final int priority = this.getPriority(priorityOverride, contracts, Feature.class, provider);
			final Feature feature = this.injectedInstance((Class<? extends Feature>) provider);
			if (feature.configure(new FeatureContextDelegate(this)))
				this.enabledFeatures.add(feature);
			this.featureClasses.add(provider);
			newContracts.put(Feature.class, priority);

		}
		if (EE8_PREVIEW_MODE && this.isA(provider, RxInvokerProvider.class, contracts)) {
			final int priority = this.getPriority(priorityOverride, contracts, RxInvokerProvider.class, provider);
			newContracts.put(RxInvokerProvider.class, priority);
		}
		if (this.isA(provider, ResponseExceptionMapper.class, contracts))
			try {
				final Object mapper = provider.newInstance();
				this.registerProviderInstance(mapper, contracts, null, false);

				if (contracts != null) {
					final Integer prio = contracts.get(ResponseExceptionMapper.class) != null
							? contracts.get(ResponseExceptionMapper.class)
							: ((ResponseExceptionMapper) mapper).getPriority();

					newContracts.put(ResponseExceptionMapper.class, prio);
				} else
					newContracts.put(ResponseExceptionMapper.class, ((ResponseExceptionMapper) mapper).getPriority());
			} catch (final Throwable e) {
				e.printStackTrace();
				throw new RuntimeException("Failed to register provider", e);
			}
		this.providerClasses.add(provider);
		this.getClassContracts().put(provider, newContracts);
	}

	/**
	 * Register a @Provider object. Can be a MessageBodyReader/Writer or
	 * ExceptionMapper.
	 *
	 * @param provider
	 */
	public void registerProviderInstance(final Object provider) {
		this.registerProviderInstance(provider, null, null, false);
	}

	public void registerProviderInstance(final Object provider, final Map<Class<?>, Integer> contracts,
			final Integer priorityOverride, final boolean builtIn) {
		for (final Object registered : this.getInstances())
			if (registered == provider) {
				LogMessages.LOGGER.providerInstanceAlreadyRegistered(provider.getClass().getName());
				return;
			}
		if (this.getClasses().contains(provider.getClass())) {
			LogMessages.LOGGER.providerClassAlreadyRegistered(provider.getClass().getName());
			return;
		}
		final Map<Class<?>, Integer> newContracts = new HashMap<>();
		if (this.isA(provider, ParamConverterProvider.class, contracts)) {
			this.injectProperties(provider);
			if (this.sortedParamConverterProviders == null)
				this.sortedParamConverterProviders = Collections
						.synchronizedSortedSet(new TreeSet<>(this.parent.getSortedParamConverterProviders()));
			final int priority = this.getPriority(priorityOverride, contracts, ParamConverterProvider.class,
					provider.getClass());
			this.sortedParamConverterProviders.add(new ExtSortedKey<>(null, (ParamConverterProvider) provider,
					provider.getClass(), priority, builtIn));
			this.paramConverterProviders = null;
			newContracts.put(ParamConverterProvider.class, priority);
		}
		if (this.isA(provider, MessageBodyReader.class, contracts))
			try {
				final int priority = this.getPriority(priorityOverride, contracts, MessageBodyReader.class,
						provider.getClass());
				this.addMessageBodyReader((MessageBodyReader) provider, priority, builtIn);
				newContracts.put(MessageBodyReader.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateMessageBodyReader(), e);
			}
		if (this.isA(provider, MessageBodyWriter.class, contracts))
			try {
				final int priority = this.getPriority(priorityOverride, contracts, MessageBodyWriter.class,
						provider.getClass());
				this.addMessageBodyWriter((MessageBodyWriter) provider, provider.getClass(), priority, builtIn);
				newContracts.put(MessageBodyWriter.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateMessageBodyWriter(), e);
			}
		if (this.isA(provider, ExceptionMapper.class, contracts))
			try {
				this.addExceptionMapper((ExceptionMapper) provider, builtIn);
				final int priority = this.getPriority(priorityOverride, contracts, ExceptionMapper.class,
						provider.getClass());
				newContracts.put(ExceptionMapper.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateExceptionMapper(), e);
			}
		if (this.isA(provider, ClientExceptionMapper.class, contracts))
			try {
				this.addClientExceptionMapper((ClientExceptionMapper) provider);
				newContracts.put(ClientExceptionMapper.class, 0);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateExceptionMapper(), e);
			}
		if (this.isA(provider, AsyncResponseProvider.class, contracts))
			try {
				this.addAsyncResponseProvider((AsyncResponseProvider) provider);
				final int priority = this.getPriority(priorityOverride, contracts, AsyncResponseProvider.class,
						provider.getClass());
				newContracts.put(AsyncResponseProvider.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateAsyncResponseProvider(), e);
			}
		if (this.isA(provider, AsyncStreamProvider.class, contracts))
			try {
				this.addAsyncStreamProvider((AsyncStreamProvider) provider);
				final int priority = this.getPriority(priorityOverride, contracts, AsyncStreamProvider.class,
						provider.getClass());
				newContracts.put(AsyncStreamProvider.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateAsyncStreamProvider(), e);
			}
		if (this.isA(provider, ContextResolver.class, contracts))
			try {
				this.addContextResolver((ContextResolver) provider);
				final int priority = this.getPriority(priorityOverride, contracts, ContextResolver.class,
						provider.getClass());
				newContracts.put(ContextResolver.class, priority);
			} catch (final Exception e) {
				throw new RuntimeException(Messages.MESSAGES.unableToInstantiateContextResolver(), e);
			}
		if (this.isA(provider, ClientRequestFilter.class, contracts)) {
			if (this.clientRequestFilterRegistry == null)
				this.clientRequestFilterRegistry = this.parent.getClientRequestFilterRegistry().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ClientRequestFilter.class,
					provider.getClass());
			this.clientRequestFilterRegistry.registerSingleton((ClientRequestFilter) provider, priority);
			newContracts.put(ClientRequestFilter.class, priority);

			{ // code maintained for backward compatibility for jaxrs-legacy code
				if (this.clientRequestFilters == null)
					this.clientRequestFilters = this.parent.getClientRequestFilters().clone(this);
				this.clientRequestFilters.registerSingleton((ClientRequestFilter) provider, priority);
			}
		}
		if (this.isA(provider, ClientResponseFilter.class, contracts)) {
			if (this.clientResponseFilters == null)
				this.clientResponseFilters = this.parent.getClientResponseFilters().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ClientResponseFilter.class,
					provider.getClass());
			this.clientResponseFilters.registerSingleton((ClientResponseFilter) provider, priority);
			newContracts.put(ClientResponseFilter.class, priority);
		}
		if (this.isA(provider, ClientExecutionInterceptor.class, contracts)) {
			if (this.clientExecutionInterceptorRegistry == null)
				this.clientExecutionInterceptorRegistry = this.parent.getClientExecutionInterceptorRegistry()
						.cloneTo(this);
			this.clientExecutionInterceptorRegistry.register((ClientExecutionInterceptor) provider);
			newContracts.put(ClientExecutionInterceptor.class, 0);
		}
		if (this.isA(provider, PreProcessInterceptor.class, contracts)) {
			if (this.containerRequestFilterRegistry == null)
				this.containerRequestFilterRegistry = this.parent.getContainerRequestFilterRegistry().clone(this);
			this.containerRequestFilterRegistry.registerLegacy((PreProcessInterceptor) provider);
			newContracts.put(PreProcessInterceptor.class, 0);
		}
		if (this.isA(provider, ContainerRequestFilter.class, contracts)) {
			if (this.containerRequestFilterRegistry == null)
				this.containerRequestFilterRegistry = this.parent.getContainerRequestFilterRegistry().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ContainerRequestFilter.class,
					provider.getClass());
			this.containerRequestFilterRegistry.registerSingleton((ContainerRequestFilter) provider, priority);
			newContracts.put(ContainerRequestFilter.class, priority);
		}
		if (this.isA(provider, PostProcessInterceptor.class, contracts)) {
			if (this.containerResponseFilterRegistry == null)
				this.containerResponseFilterRegistry = this.parent.getContainerResponseFilterRegistry().clone(this);
			this.containerResponseFilterRegistry.registerLegacy((PostProcessInterceptor) provider);
			newContracts.put(PostProcessInterceptor.class, 0);
		}
		if (this.isA(provider, ContainerResponseFilter.class, contracts)) {
			if (this.containerResponseFilterRegistry == null)
				this.containerResponseFilterRegistry = this.parent.getContainerResponseFilterRegistry().clone(this);
			final int priority = this.getPriority(priorityOverride, contracts, ContainerResponseFilter.class,
					provider.getClass());
			this.containerResponseFilterRegistry.registerSingleton((ContainerResponseFilter) provider, priority);
			newContracts.put(ContainerResponseFilter.class, priority);
		}
		if (this.isA(provider, ReaderInterceptor.class, contracts)) {
			final ConstrainedTo constrainedTo = provider.getClass().getAnnotation(ConstrainedTo.class);
			final int priority = this.getPriority(priorityOverride, contracts, ReaderInterceptor.class,
					provider.getClass());
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.SERVER) {
				if (this.serverReaderInterceptorRegistry == null)
					this.serverReaderInterceptorRegistry = this.parent.getServerReaderInterceptorRegistry().clone(this);
				this.serverReaderInterceptorRegistry.registerSingleton((ReaderInterceptor) provider, priority);
			}
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.CLIENT) {
				if (this.clientReaderInterceptorRegistry == null)
					this.clientReaderInterceptorRegistry = this.parent.getClientReaderInterceptorRegistry().clone(this);
				this.clientReaderInterceptorRegistry.registerSingleton((ReaderInterceptor) provider, priority);
			}
			if (constrainedTo == null) {
				if (this.serverReaderInterceptorRegistry == null)
					this.serverReaderInterceptorRegistry = this.parent.getServerReaderInterceptorRegistry().clone(this);
				this.serverReaderInterceptorRegistry.registerSingleton((ReaderInterceptor) provider, priority);
				if (this.clientReaderInterceptorRegistry == null)
					this.clientReaderInterceptorRegistry = this.parent.getClientReaderInterceptorRegistry().clone(this);
				this.clientReaderInterceptorRegistry.registerSingleton((ReaderInterceptor) provider, priority);
			}
			newContracts.put(ReaderInterceptor.class, priority);
		}
		if (this.isA(provider, WriterInterceptor.class, contracts)) {
			final ConstrainedTo constrainedTo = provider.getClass().getAnnotation(ConstrainedTo.class);
			final int priority = this.getPriority(priorityOverride, contracts, WriterInterceptor.class,
					provider.getClass());
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.SERVER) {
				if (this.serverWriterInterceptorRegistry == null)
					this.serverWriterInterceptorRegistry = this.parent.getServerWriterInterceptorRegistry().clone(this);
				this.serverWriterInterceptorRegistry.registerSingleton((WriterInterceptor) provider, priority);
			}
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.CLIENT) {
				if (this.clientWriterInterceptorRegistry == null)
					this.clientWriterInterceptorRegistry = this.parent.getClientWriterInterceptorRegistry().clone(this);
				this.clientWriterInterceptorRegistry.registerSingleton((WriterInterceptor) provider, priority);
			}
			if (constrainedTo == null) {
				if (this.serverWriterInterceptorRegistry == null)
					this.serverWriterInterceptorRegistry = this.parent.getServerWriterInterceptorRegistry().clone(this);
				this.serverWriterInterceptorRegistry.registerSingleton((WriterInterceptor) provider, priority);
				if (this.clientWriterInterceptorRegistry == null)
					this.clientWriterInterceptorRegistry = this.parent.getClientWriterInterceptorRegistry().clone(this);
				this.clientWriterInterceptorRegistry.registerSingleton((WriterInterceptor) provider, priority);
			}
			newContracts.put(WriterInterceptor.class, priority);
		}
		if (this.isA(provider, MessageBodyWriterInterceptor.class, contracts)) {
			if (provider.getClass().isAnnotationPresent(ServerInterceptor.class)) {
				if (this.serverWriterInterceptorRegistry == null)
					this.serverWriterInterceptorRegistry = this.parent.getServerWriterInterceptorRegistry().clone(this);
				this.serverWriterInterceptorRegistry.registerLegacy((MessageBodyWriterInterceptor) provider);
			}
			if (provider.getClass().isAnnotationPresent(ClientInterceptor.class)) {
				if (this.clientWriterInterceptorRegistry == null)
					this.clientWriterInterceptorRegistry = this.parent.getClientWriterInterceptorRegistry().clone(this);
				this.clientWriterInterceptorRegistry.registerLegacy((MessageBodyWriterInterceptor) provider);
			}
			if (!provider.getClass().isAnnotationPresent(ServerInterceptor.class)
					&& !provider.getClass().isAnnotationPresent(ClientInterceptor.class))
				throw new RuntimeException(
						Messages.MESSAGES.interceptorClassMustBeAnnotatedWithClass(provider.getClass()));
			newContracts.put(MessageBodyWriterInterceptor.class, 0);
		}
		if (this.isA(provider, MessageBodyReaderInterceptor.class, contracts)) {
			if (provider.getClass().isAnnotationPresent(ServerInterceptor.class)) {
				if (this.serverReaderInterceptorRegistry == null)
					this.serverReaderInterceptorRegistry = this.parent.getServerReaderInterceptorRegistry().clone(this);
				this.serverReaderInterceptorRegistry.registerLegacy((MessageBodyReaderInterceptor) provider);
			}
			if (provider.getClass().isAnnotationPresent(ClientInterceptor.class)) {
				if (this.clientReaderInterceptorRegistry == null)
					this.clientReaderInterceptorRegistry = this.parent.getClientReaderInterceptorRegistry().clone(this);
				this.clientReaderInterceptorRegistry.registerLegacy((MessageBodyReaderInterceptor) provider);
			}
			if (!provider.getClass().isAnnotationPresent(ServerInterceptor.class)
					&& !provider.getClass().isAnnotationPresent(ClientInterceptor.class))
				throw new RuntimeException(
						Messages.MESSAGES.interceptorClassMustBeAnnotatedWithClass(provider.getClass()));
			newContracts.put(MessageBodyReaderInterceptor.class, 0);

		}
		if (this.isA(provider, StringConverter.class, contracts)) {
			this.addStringConverter((StringConverter) provider);
			newContracts.put(StringConverter.class, 0);
		}
		if (this.isA(provider, InjectorFactory.class, contracts)) {
			this.injectorFactory = (InjectorFactory) provider;
			newContracts.put(InjectorFactory.class, 0);
		}
		if (this.isA(provider, DynamicFeature.class, contracts)) {
			final ConstrainedTo constrainedTo = provider.getClass().getAnnotation(ConstrainedTo.class);
			final int priority = this.getPriority(priorityOverride, contracts, DynamicFeature.class,
					provider.getClass());
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.SERVER) {
				if (this.serverDynamicFeatures == null)
					this.serverDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getServerDynamicFeatures());
				this.serverDynamicFeatures.add((DynamicFeature) provider);
			}
			if (constrainedTo != null && constrainedTo.value() == RuntimeType.CLIENT) {
				if (this.clientDynamicFeatures == null)
					this.clientDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getClientDynamicFeatures());
				this.clientDynamicFeatures.add((DynamicFeature) provider);
			}
			if (constrainedTo == null) {
				if (this.serverDynamicFeatures == null)
					this.serverDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getServerDynamicFeatures());
				this.serverDynamicFeatures.add((DynamicFeature) provider);
				if (this.clientDynamicFeatures == null)
					this.clientDynamicFeatures = new CopyOnWriteArraySet<>(this.parent.getClientDynamicFeatures());
				this.clientDynamicFeatures.add((DynamicFeature) provider);
			}
			newContracts.put(DynamicFeature.class, priority);
		}
		if (this.isA(provider, Feature.class, contracts)) {
			final Feature feature = (Feature) provider;
			this.injectProperties(provider.getClass(), provider);
			if (feature.configure(new FeatureContextDelegate(this)))
				this.enabledFeatures.add(feature);
			this.featureInstances.add(provider);
			final int priority = this.getPriority(priorityOverride, contracts, Feature.class, provider.getClass());
			newContracts.put(Feature.class, priority);

		}
		if (this.isA(provider, ResponseExceptionMapper.class, contracts))
			if (contracts != null) {
				final Integer prio = contracts.get(ResponseExceptionMapper.class) != null
						? contracts.get(ResponseExceptionMapper.class)
						: ((ResponseExceptionMapper) provider).getPriority();
				newContracts.put(ResponseExceptionMapper.class, prio);
			} else
				newContracts.put(ResponseExceptionMapper.class, ((ResponseExceptionMapper) provider).getPriority());
		this.providerInstances.add(provider);
		this.getClassContracts().put(provider.getClass(), newContracts);
	}

	@Override
	public <T extends Throwable> ExceptionMapper<T> getExceptionMapper(final Class<T> type) {
		Class exceptionType = type;
		SortedKey<ExceptionMapper> mapper = null;
		while (mapper == null) {
			if (exceptionType == null)
				break;
			mapper = this.getSortedExceptionMappers().get(exceptionType);
			if (mapper == null)
				exceptionType = exceptionType.getSuperclass();
		}
		return mapper != null ? mapper.getObj() : null;
	}

	public <T extends Throwable> ClientExceptionMapper<T> getClientExceptionMapper(final Class<T> type) {
		return this.getClientExceptionMappers().get(type);
	}

	// @Override
	public <T> AsyncResponseProvider<T> getAsyncResponseProvider(final Class<T> type) {
		Class asyncType = type;
		AsyncResponseProvider<T> mapper = null;
		while (mapper == null) {
			if (asyncType == null)
				break;
			mapper = this.getAsyncResponseProviders().get(asyncType);
			if (mapper == null)
				asyncType = asyncType.getSuperclass();
		}
		return mapper;
	}

	// @Override
	public <T> AsyncStreamProvider<T> getAsyncStreamProvider(final Class<T> type) {
		Class asyncType = type;
		AsyncStreamProvider<T> mapper = null;
		while (mapper == null) {
			if (asyncType == null)
				break;
			mapper = this.getAsyncStreamProviders().get(asyncType);
			if (mapper == null)
				asyncType = asyncType.getSuperclass();
		}
		return mapper;
	}

	public MediaType getConcreteMediaTypeFromMessageBodyWriters(final Class type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		final List<SortedKey<MessageBodyWriter>> writers = this.getServerMessageBodyWriters().getPossible(mediaType,
				type);
		for (final SortedKey<MessageBodyWriter> writer : writers)
			if (writer.obj.isWriteable(type, genericType, annotations, mediaType)) {
				final MessageBodyWriter mbw = writer.obj;
				final Class writerType = Types.getTemplateParameterOfInterface(mbw.getClass(), MessageBodyWriter.class);
				if (writerType == null || writerType.equals(Object.class) || !writerType.isAssignableFrom(type))
					continue;
				final Produces produces = mbw.getClass().getAnnotation(Produces.class);
				if (produces == null)
					continue;
				for (final String produce : produces.value()) {
					final MediaType mt = MediaType.valueOf(produce);
					if (mt.isWildcardType() || mt.isWildcardSubtype())
						continue;
					return mt;
				}
			}
		return null;
	}

	public Map<MessageBodyWriter<?>, Class<?>> getPossibleMessageBodyWritersMap(final Class type,
			final Type genericType, final Annotation[] annotations, final MediaType accept) {
		final Map<MessageBodyWriter<?>, Class<?>> map = new HashMap<>();
		final List<SortedKey<MessageBodyWriter>> writers = this.getServerMessageBodyWriters().getPossible(accept, type);
		for (final SortedKey<MessageBodyWriter> writer : writers)
			if (writer.obj.isWriteable(type, genericType, annotations, accept)) {
				Class<?> mbwc = writer.obj.getClass();
				if (!mbwc.isInterface() && mbwc.getSuperclass() != null && !mbwc.getSuperclass().equals(Object.class)
						&& mbwc.isSynthetic())
					mbwc = mbwc.getSuperclass();
				final Class writerType = Types.getTemplateParameterOfInterface(mbwc, MessageBodyWriter.class);
				if (writerType == null || !writerType.isAssignableFrom(type))
					continue;
				map.put(writer.obj, writerType);
			}
		return map;
	}

	public <T> MessageBodyWriter<T> getServerMessageBodyWriter(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		final MediaTypeMap<SortedKey<MessageBodyWriter>> availableWriters = this.getServerMessageBodyWriters();
		return this.resolveMessageBodyWriter(type, genericType, annotations, mediaType, availableWriters);
	}

	/**
	 * Always gets server MBW
	 *
	 * @param type
	 *            the class of the object that is to be written.
	 * @param genericType
	 *            the type of object to be written. E.g. if the message body is to
	 *            be produced from a field, this will be the declared type of the
	 *            field as returned by {@code Field.getGenericType}.
	 * @param annotations
	 *            an array of the annotations on the declaration of the artifact
	 *            that will be written. E.g. if the message body is to be produced
	 *            from a field, this will be the annotations on that field returned
	 *            by {@code Field.getDeclaredAnnotations}.
	 * @param mediaType
	 *            the media type of the data that will be written.
	 * @param <T>
	 * @return
	 */
	@Override
	public <T> MessageBodyWriter<T> getMessageBodyWriter(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		final MediaTypeMap<SortedKey<MessageBodyWriter>> availableWriters = this.getServerMessageBodyWriters();
		final MessageBodyWriter<T> writer = this.resolveMessageBodyWriter(type, genericType, annotations, mediaType,
				availableWriters);
		if (writer != null)
			LogMessages.LOGGER.debugf("MessageBodyWriter: %s", writer.getClass().getName());
		return writer;
	}

	public <T> MessageBodyWriter<T> getClientMessageBodyWriter(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType) {
		final MediaTypeMap<SortedKey<MessageBodyWriter>> availableWriters = this.getClientMessageBodyWriters();
		return this.resolveMessageBodyWriter(type, genericType, annotations, mediaType, availableWriters);
	}

	protected <T> MessageBodyWriter<T> resolveMessageBodyWriter(final Class<T> type, final Type genericType,
			final Annotation[] annotations, final MediaType mediaType,
			final MediaTypeMap<SortedKey<MessageBodyWriter>> availableWriters) {
		final List<SortedKey<MessageBodyWriter>> writers = availableWriters.getPossible(mediaType, type);
		/*
		 * logger.info("*******   getMessageBodyWriter(" + type.getName() + ", " +
		 * mediaType.toString() + ")****"); for (SortedKey<MessageBodyWriter> writer :
		 * writers) { logger.info("     possible writer: " +
		 * writer.obj.getClass().getName()); }
		 */

		for (final SortedKey<MessageBodyWriter> writer : writers)
			if (writer.obj.isWriteable(type, genericType, annotations, mediaType)) {
				LogMessages.LOGGER.debugf("MessageBodyWriter: %s", writer.getClass().getName());
				// logger.info(" picking: " + writer.obj.getClass().getName());
				return writer.obj;
			}
		return null;
	}

	/**
	 * this is a spec method that is unsupported. it is an optional method anyways.
	 *
	 * @param applicationConfig
	 * @param endpointType
	 * @return
	 * @throws IllegalArgumentException
	 * @throws UnsupportedOperationException
	 */
	@Override
	public <T> T createEndpoint(final Application applicationConfig, final Class<T> endpointType)
			throws IllegalArgumentException, UnsupportedOperationException {
		if (applicationConfig == null)
			throw new IllegalArgumentException(Messages.MESSAGES.applicationParamNull());
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> ContextResolver<T> getContextResolver(final Class<T> contextType, final MediaType mediaType) {
		final List<ContextResolver> resolvers = this.getContextResolvers(contextType, mediaType);
		if (resolvers == null)
			return null;
		if (resolvers.size() == 1)
			return resolvers.get(0);
		return type -> {
			for (final ContextResolver resolver : resolvers) {
				final Object rtn = resolver.getContext(type);
				if (rtn != null)
					return (T) rtn;
			}
			return null;
		};
	}

	/**
	 * Create an instance of a class using provider allocation rules of the
	 * specification as well as the InjectorFactory
	 * <p/>
	 * only does constructor injection
	 *
	 * @param clazz
	 * @param <T>
	 * @return
	 */
	public <T> T createProviderInstance(final Class<? extends T> clazz) {
		final ConstructorInjector constructorInjector = this.createConstructorInjector(clazz);

		final T provider = (T) constructorInjector.construct();
		return provider;
	}

	public <T> ConstructorInjector createConstructorInjector(final Class<? extends T> clazz) {
		final Constructor<?> constructor = PickConstructor.pickSingletonConstructor(clazz);
		if (constructor == null)
			throw new IllegalArgumentException(
					Messages.MESSAGES.unableToFindPublicConstructorForProvider(clazz.getName()));
		return this.getInjectorFactory().createConstructor(constructor, this);
	}

	/**
	 * Property and constructor injection using the InjectorFactory
	 *
	 * @param clazz
	 * @param <T>
	 * @return
	 */
	public <T> T injectedInstance(final Class<? extends T> clazz) {
		final Constructor<?> constructor = PickConstructor.pickSingletonConstructor(clazz);
		Object obj = null;
		final ConstructorInjector constructorInjector = this.getInjectorFactory().createConstructor(constructor, this);
		obj = constructorInjector.construct();

		final PropertyInjector propertyInjector = this.getInjectorFactory().createPropertyInjector(clazz, this);

		propertyInjector.inject(obj);
		return (T) obj;
	}

	/**
	 * Property and constructor injection using the InjectorFactory
	 *
	 * @param clazz
	 * @param <T>
	 * @return
	 */
	public <T> T injectedInstance(final Class<? extends T> clazz, final HttpRequest request,
			final HttpResponse response) {
		Constructor<?> constructor = PickConstructor.pickSingletonConstructor(clazz);
		Object obj = null;
		if (constructor == null) {
			// TODO this is solely to pass the TCK. This is WRONG WRONG WRONG! I'm
			// challenging.
			if (false)// if (clazz.isAnonymousClass())
			{
				constructor = clazz.getDeclaredConstructors()[0];
				constructor.setAccessible(true);
				if (!Modifier.isStatic(clazz.getModifiers())) {
					final Object[] args = { null };
					try {
						obj = constructor.newInstance(args);
					} catch (final InstantiationException e) {
						throw new RuntimeException(e);
					} catch (final IllegalAccessException e) {
						throw new RuntimeException(e);
					} catch (final InvocationTargetException e) {
						throw new RuntimeException(e);
					}
				} else
					try {
						obj = constructor.newInstance();
					} catch (final InstantiationException e) {
						throw new RuntimeException(e);
					} catch (final IllegalAccessException e) {
						throw new RuntimeException(e);
					} catch (final InvocationTargetException e) {
						throw new RuntimeException(e);
					}
			} else
				throw new IllegalArgumentException(
						Messages.MESSAGES.unableToFindPublicConstructorForClass(clazz.getName()));
		} else {
			final ConstructorInjector constructorInjector = this.getInjectorFactory().createConstructor(constructor,
					this);
			obj = constructorInjector.construct(request, response);

		}
		final PropertyInjector propertyInjector = this.getInjectorFactory().createPropertyInjector(clazz, this);

		propertyInjector.inject(request, response, obj);
		return (T) obj;
	}

	public void injectProperties(final Class declaring, final Object obj) {
		this.getInjectorFactory().createPropertyInjector(declaring, this).inject(obj);
	}

	public void injectProperties(final Object obj) {
		this.getInjectorFactory().createPropertyInjector(obj.getClass(), this).inject(obj);
	}

	public void injectProperties(final Object obj, final HttpRequest request, final HttpResponse response) {
		this.getInjectorFactory().createPropertyInjector(obj.getClass(), this).inject(request, response, obj);
	}

	// Configurable

	public Map<String, Object> getMutableProperties() {
		return this.properties;
	}

	@Override
	public Map<String, Object> getProperties() {
		return Collections.unmodifiableMap(this.properties);
	}

	@Override
	public Object getProperty(final String name) {
		return this.properties.get(name);
	}

	public ResteasyProviderFactory setProperties(final Map<String, ?> properties) {
		final Map<String, Object> newProp = new ConcurrentHashMap<>();
		newProp.putAll(properties);
		this.properties = newProp;
		return this;
	}

	@Override
	public ResteasyProviderFactory property(final String name, final Object value) {
		if (value == null)
			this.properties.remove(name);
		else
			this.properties.put(name, value);
		return this;
	}

	public Collection<Feature> getEnabledFeatures() {
		if (this.enabledFeatures == null && this.parent != null)
			return this.parent.getEnabledFeatures();
		final Set<Feature> set = new HashSet<>();
		if (this.parent != null)
			set.addAll(this.parent.getEnabledFeatures());
		set.addAll(this.enabledFeatures);
		return set;
	}

	public Set<Class<?>> getFeatureClasses() {
		if (this.featureClasses == null && this.parent != null)
			return this.parent.getFeatureClasses();
		final Set<Class<?>> set = new HashSet<>();
		if (this.parent != null)
			set.addAll(this.parent.getFeatureClasses());
		set.addAll(this.featureClasses);
		return set;
	}

	public Set<Object> getFeatureInstances() {
		if (this.featureInstances == null && this.parent != null)
			return this.parent.getFeatureInstances();
		final Set<Object> set = new HashSet<>();
		if (this.parent != null)
			set.addAll(this.parent.getFeatureInstances());
		set.addAll(this.featureInstances);
		return set;
	}

	@Override
	public ResteasyProviderFactory register(final Class<?> providerClass) {
		this.registerProvider(providerClass);
		return this;
	}

	@Override
	public ResteasyProviderFactory register(final Object provider) {
		this.registerProviderInstance(provider);
		return this;
	}

	@Override
	public ResteasyProviderFactory register(final Class<?> componentClass, final int priority) {
		this.registerProvider(componentClass, priority, false, null);
		return this;
	}

	@Override
	public ResteasyProviderFactory register(final Class<?> componentClass, final Class<?>... contracts) {
		if (contracts == null || contracts.length == 0) {
			LogMessages.LOGGER.attemptingToRegisterEmptyContracts(componentClass.getName());
			return this;
		}
		final Map<Class<?>, Integer> cons = new HashMap<>();
		for (final Class<?> contract : contracts) {
			if (!contract.isAssignableFrom(componentClass)) {
				LogMessages.LOGGER.attemptingToRegisterUnassignableContract(componentClass.getName());
				return this;
			}
			cons.put(contract, Priorities.USER);
		}
		this.registerProvider(componentClass, null, false, cons);
		return this;
	}

	@Override
	public ResteasyProviderFactory register(final Object component, final int priority) {
		this.registerProviderInstance(component, null, priority, false);
		return this;
	}

	@Override
	public ResteasyProviderFactory register(final Object component, final Class<?>... contracts) {
		if (contracts == null || contracts.length == 0) {
			LogMessages.LOGGER.attemptingToRegisterEmptyContracts(component.getClass().getName());
			return this;
		}
		final Map<Class<?>, Integer> cons = new HashMap<>();
		for (final Class<?> contract : contracts) {
			if (!contract.isAssignableFrom(component.getClass())) {
				LogMessages.LOGGER.attemptingToRegisterUnassignableContract(component.getClass().getName());
				return this;
			}
			cons.put(contract, Priorities.USER);
		}
		this.registerProviderInstance(component, cons, null, false);
		return this;
	}

	@Override
	public ResteasyProviderFactory register(final Class<?> componentClass, final Map<Class<?>, Integer> contracts) {
		for (final Class<?> contract : contracts.keySet())
			if (!contract.isAssignableFrom(componentClass)) {
				LogMessages.LOGGER.attemptingToRegisterUnassignableContract(componentClass.getName());
				return this;
			}
		this.registerProvider(componentClass, null, false, contracts);
		return this;
	}

	@Override
	public ResteasyProviderFactory register(final Object component, final Map<Class<?>, Integer> contracts) {
		for (final Class<?> contract : contracts.keySet())
			if (!contract.isAssignableFrom(component.getClass())) {
				LogMessages.LOGGER.attemptingToRegisterUnassignableContract(component.getClass().getName());
				return this;
			}
		this.registerProviderInstance(component, contracts, null, false);
		return this;
	}

	@Override
	public Configuration getConfiguration() {
		return this;
	}

	@Override
	public RuntimeType getRuntimeType() {
		return RuntimeType.SERVER;
	}

	@Override
	public Collection<String> getPropertyNames() {
		return this.getProperties().keySet();
	}

	@Override
	public boolean isEnabled(final Feature feature) {
		final Collection<Feature> enabled = this.getEnabledFeatures();
		// logger.info("********* isEnabled(Feature): " + feature.getClass().getName() +
		// " # enabled: " + enabled.size());
		for (final Feature f : enabled)
			// logger.info(" looking at: " + f.getClass());
			if (f == feature)
				// logger.info(" found: " + f.getClass().getName());
				return true;
		return false;
	}

	@Override
	public boolean isEnabled(final Class<? extends Feature> featureClass) {
		final Collection<Feature> enabled = this.getEnabledFeatures();
		// logger.info("isEnabled(Class): " + featureClass.getName() + " # enabled: " +
		// enabled.size());
		if (enabled == null)
			return false;
		for (final Feature feature : enabled)
			// logger.info(" looking at: " + feature.getClass());
			if (featureClass.equals(feature.getClass()))
				// logger.info(" found: " + featureClass.getName());
				return true;
		// logger.info("not enabled class: " + featureClass.getName());
		return false;
	}

	@Override
	public boolean isRegistered(final Object component) {
		return this.getProviderInstances().contains(component);
	}

	@Override
	public boolean isRegistered(final Class<?> componentClass) {
		if (this.getProviderClasses().contains(componentClass))
			return true;
		for (final Object obj : this.getProviderInstances())
			if (obj.getClass().equals(componentClass))
				return true;
		return false;
	}

	@Override
	public Map<Class<?>, Integer> getContracts(final Class<?> componentClass) {
		if (this.classContracts == null && this.parent == null)
			return Collections.emptyMap();
		else if (this.classContracts == null)
			return this.parent.getContracts(componentClass);
		else {
			final Map<Class<?>, Integer> classIntegerMap = this.classContracts.get(componentClass);
			if (classIntegerMap == null)
				return Collections.emptyMap();
			return classIntegerMap;
		}
	}

	@Override
	public Set<Class<?>> getClasses() {
		return this.getProviderClasses();
	}

	@Override
	public Set<Object> getInstances() {
		return this.getProviderInstances();
	}

	@Override
	public Link.Builder createLinkBuilder() {
		return new LinkBuilderImpl();
	}

	@Override
	public JAXRS.Configuration.Builder createConfigurationBuilder() {
		return new JAXRS.Configuration.Builder() {
			private final Map<String, Object> properties = new HashMap<>();

			{
				this.properties.put(JAXRS.Configuration.PROTOCOL, "HTTP");
				this.properties.put(JAXRS.Configuration.HOST, "localhost");
				this.properties.put(JAXRS.Configuration.PORT, -1); // Auto-select port 80 for HTTP or 443 for HTTPS
				this.properties.put(JAXRS.Configuration.ROOT_PATH, "/");
				try {
					this.properties.put(JAXRS.Configuration.SSL_CONTEXT, SSLContext.getDefault());
				} catch (final NoSuchAlgorithmException e) {
					throw new RuntimeException(e);
				}
				this.properties.put(JAXRS.Configuration.SSL_CLIENT_AUTHENTICATION,
						JAXRS.Configuration.SSLClientAuthentication.NONE);
			}

			@Override
			public final JAXRS.Configuration.Builder property(final String name, final Object value) {
				this.properties.put(name, value);
				return this;
			}

			@Override
			public final JAXRS.Configuration build() {
				return new JAXRS.Configuration() {
					@Override
					public final Object property(final String name) {
						return properties.get(name);
					}
				};
			}
		};
	}

	@SuppressWarnings("unchecked")
	@Override
	public CompletionStage<JAXRS.Instance> bootstrap(final Application application,
			final JAXRS.Configuration configuration) {
		return CompletableFuture.supplyAsync(() -> {
			final String protocol = configuration.protocol();
			final String host = configuration.host();
			final int port = configuration.port();
			final String rootPath = configuration.rootPath();
			final SSLContext sslContext = configuration.sslContext();
			configuration.sslClientAuthentication();

			final ResteasyDeployment resteasyDeployment = new ResteasyDeployment();
			resteasyDeployment.setApplication(application);

			final NettyJaxrsServer netty = new NettyJaxrsServer();
			netty.setHostname(host);
			netty.setPort(port == JAXRS.Configuration.DEFAULT_PORT ? "HTTPS".equals(protocol) ? 443 : 80 : port);
			netty.setRootResourcePath(rootPath);
			netty.setDeployment(resteasyDeployment);
			netty.setSSLContext("HTTPS".equals(protocol) ? sslContext : null);
			netty.start();

			return new JAXRS.Instance() {
				@Override
				public final JAXRS.Configuration configuration() {
					return name -> {
						switch (name) {
						case JAXRS.Configuration.PORT:
							return netty.getPort();
						case JAXRS.Configuration.HOST:
							return netty.getHostname();
						default:
							return configuration.property(name);
						}
					};
				}

				@Override
				public final CompletionStage<StopResult> stop() {
					return CompletableFuture.runAsync(netty::stop).thenApply(nativeResult -> new StopResult() {

						@Override
						public final <T> T unwrap(final Class<T> nativeClass) {
							return nativeClass.cast(nativeResult);
						}
					});
				}

				@Override
				public final <T> T unwrap(final Class<T> nativeClass) {
					return nativeClass.cast(netty);
				}
			};
		});
	}

}
